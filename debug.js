//debug.js

var uFs = require('fs')
var uAsync = require('./node_modules/async/lib/async')
var uMisc = require('./misc')
var uDate = require('./myDate')

var ex = module.exports

var logLevel = 2
var logFileName = "s_log.txt"
var pathToPlayersLogs = 'data/logs/players/'

var MAX_OPEN_FILES = 128

ex.__ConsoleLog = function(filename)
{  
   for (var i = 1; i < arguments.length; i++)
   {
      allArgs.push(arguments[i])
   }
   console.log.apply(null,allArgs)
}

ex.__FileLog = function(filename)
{
   var topArgs = arguments
   return function(callback)
   {
      var date = new uDate.MyDate(uMisc.GetServerTime())
      var allData = uDate.GetFullTimeString(date)+": "
      for (var i = 1; i < topArgs.length; i++)
      {
         allData += topArgs[i]
         if (i < topArgs.length-1)
            allData += " "
         else
            allData += "\n"
      }
      uFs.open(filename,"a",function(error,file)
      {
         if (!error)
         {
            uFs.write(file,allData,null,'utf8', function(err)
            {
               if (err)
               {
                  console.log(err)
                  console.log(err.stack)
               }
               uFs.close(file, function() { callback(); });
            })
         }
         else
         {
            console.log(error)
            console.log(error.stack)
            callback()
         }
      })
   }
}

ex.__Log = ex.__FileLog
//ex.__Log = ex.__ConsoleLog

ex.LogSyncObj = function(obj)
{
   for (var key in obj)
   {
      ex.LogSyncAll(key,'=',obj[key])
   }
}

// ex.Log = function()
// {
   // if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      // return      
   // if (arguments[0] !== '_ii' && logLevel === 2)
      // return            
   
   // var allData = ""
   // for (var i = 0; i < arguments.length; i++)
   // {
      // allData += arguments[i]
      // if (i < arguments.length-1)
         // allData += " "
      // else
         // allData += "\n"
   // }  
   // uFs.appendFile(logFileName,allData)   
// }

ex.Log = function()
{
   //var args = Array.prototype.slice.call(arguments)
   //console.log.apply(console,args)
   if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      return      
   if (arguments[0] !== '_ii' && logLevel === 2)
      return         
   var args = []
   args.push(logFileName)
   args = args.concat(Array.prototype.slice.call(arguments))
   ex.__writeQueue.push(ex.__Log.apply(null,args))
}

ex.LogToPlayerLog = function(id)
{
   var args = []
   args.push(pathToPlayersLogs+id)
   args = args.concat(Array.prototype.slice.call(arguments,1))
   ex.__writeQueue.push(ex.__Log.apply(null,args))
}

ex.LogSync = function()
{
   if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      return      
   if (arguments[0] !== '_ii' && logLevel === 2)
      return    
   var allData = ""
   for (var i = 0; i < arguments.length; i++)
   {
      allData += arguments[i]
      if (i < arguments.length-1)
         allData += " "
      else
         allData += "\n"
   }   
   uFs.appendFileSync(logFileName,allData)
}

ex.LogSyncAll = function()
{
   var allData = ""
   for (var i = 0; i < arguments.length; i++)
   {
      allData += arguments[i]
      if (i < arguments.length-1)
         allData += " "
      else
         allData += "\n"
   }   
   uFs.appendFileSync(logFileName,allData)   
}

//ex.Log = ex.LogSync

ex.Init = function(a_logFileName)
{
   if (a_logFileName)
      logFileName = a_logFileName
   uFs.appendFileSync(logFileName,"\n\n------------------------------------------------------------\n")
   uFs.appendFileSync(logFileName,"SERVER RESTARTED\n\n")
}

ex.ClearLog = function()
{
   uFs.writeFileSync(logFileName,"Log Cleared\n")
}

ex.ClearPlayerLogs = function()
{
   var playersLogs = uFs.readdirSync(pathToPlayersLogs)
   for (var i = 0; i < playersLogs.length; i++)
   {
      uFs.unlinkSync(pathToPlayersLogs+playersLogs[i])
   }   
}

ex.__writeQueue = uAsync.queue(function (task, callback)
{
   task(callback)
}, MAX_OPEN_FILES)