var uBig = require('big.js')

var bigs = module.exports

bigs.ZERO = new uBig(0)
bigs.ONE_FOURTH = new uBig(0.25)
bigs.HALF = new uBig(0.5)
bigs.ONE = new uBig(1)
bigs.ONE_POINT_ONE = new uBig(1.1)
bigs.TWO = new uBig(2)
bigs.THREE = new uBig(3)
bigs.FIVE = new uBig(5)
bigs.TEN = new uBig(10)
bigs.THIRTY = new uBig(30)
bigs.THIRTY_TWO = new uBig(32)
bigs.SIXTY = new uBig(60)
bigs.ONE_HUNDRED = new uBig(100)
bigs.BIG_150 = new uBig(150)