var uMisc = require('./misc')
var uBig = require('big.js')
var uDbg = require('./debug')
var uPreferences = require('./preferences')
var ex = module.exports

var PRECREATE_LEVELS = 26
var SKILLS_EVERY_X_LEVELS = 5

ex.SKILL_INCREASE_GLOBAL_DAMAGE = 0
ex.SKILL_INCREASE_MONSTER_COINS = 1
ex.SKILL_INCREASE_STUN_CHANCE = 2
ex.SKILL_EMPOWER_HERO = 3
ex.SKILL_HEAL_CHANCE = 4
ex.SKILL_PATHFINDER = 5
ex.SKILL_MERCENARY = 6

ex.SKILL_DECREASE_MONSTER_ARMOR = 7

ex.NUMBER_OF_SKILLS = 7

ex.skills = []

var commonSkills = [ ex.SKILL_INCREASE_GLOBAL_DAMAGE, /* ex.SKILL_DECREASE_MONSTER_ARMOR,*/ ex.SKILL_INCREASE_MONSTER_COINS, ex.SKILL_INCREASE_STUN_CHANCE, ex.SKILL_EMPOWER_HERO, ex.SKILL_HEAL_CHANCE ]
var rareSkills = [ ex.SKILL_PATHFINDER, ex.SKILL_MERCENARY ]

ex.skillPacks = []
ex.levelsOfSkills = []

ex.levelsOfSkills[0] = 10
ex.levelsOfSkills[1] = 25
ex.levelsOfSkills[2] = 50
ex.levelsOfSkills[3] = 100
ex.levelsOfSkills[4] = 200
ex.levelsOfSkills[5] = 300
ex.levelsOfSkills[6] = 500
ex.levelsOfSkills[7] = 1000
ex.levelsOfSkills[8] = 1500
ex.levelsOfSkills[9] = 2000
ex.levelsOfSkills[10] = 2500
ex.levelsOfSkills[11] = 3000
ex.levelsOfSkills[12] = 3500
ex.levelsOfSkills[13] = 4000
ex.levelsOfSkills[14] = 5000
ex.levelsOfSkills[15] = 6000
ex.levelsOfSkills[16] = 7000
ex.levelsOfSkills[17] = 8000
ex.levelsOfSkills[18] = 9000 
ex.levelsOfSkills[19] = 1000 
ex.levelsOfSkills[20] = 12000
ex.levelsOfSkills[21] = 14000
ex.levelsOfSkills[22] = 16000
ex.levelsOfSkills[23] = 18000
ex.levelsOfSkills[24] = 20000
ex.levelsOfSkills[25] = 23000
ex.levelsOfSkills[26] = 26000
ex.levelsOfSkills[27] = 30000
ex.levelsOfSkills[28] = 35000
ex.levelsOfSkills[29] = 40000
ex.levelsOfSkills[30] = 50000
ex.levelsOfSkills[31] = 60000

ex.skillPrices = {}   
ex.skillPrices[ex.levelsOfSkills[0]] = new uBig(10)
ex.skillPrices[ex.levelsOfSkills[1]] = new uBig(50)
ex.skillPrices[ex.levelsOfSkills[2]] = new uBig(100)
ex.skillPrices[ex.levelsOfSkills[3]] = new uBig(200)
ex.skillPrices[ex.levelsOfSkills[4]] = new uBig(300)
ex.skillPrices[ex.levelsOfSkills[5]] = new uBig(400)
ex.skillPrices[ex.levelsOfSkills[6]] = new uBig(500)
ex.skillPrices[ex.levelsOfSkills[7]] = new uBig(600)
ex.skillPrices[ex.levelsOfSkills[8]] = new uBig(750)
ex.skillPrices[ex.levelsOfSkills[9]] = new uBig(1000)
ex.skillPrices[ex.levelsOfSkills[10]] = new uBig(1500)
ex.skillPrices[ex.levelsOfSkills[11]] = new uBig(2000)
ex.skillPrices[ex.levelsOfSkills[12]] = new uBig(2500)
ex.skillPrices[ex.levelsOfSkills[13]] = new uBig(2700)
ex.skillPrices[ex.levelsOfSkills[14]] = new uBig(3000)
ex.skillPrices[ex.levelsOfSkills[15]] = new uBig(5000)
ex.skillPrices[ex.levelsOfSkills[16]] = new uBig(10000)
ex.skillPrices[ex.levelsOfSkills[17]] = new uBig(100000)
ex.skillPrices[ex.levelsOfSkills[18]] = new uBig(200000)
ex.skillPrices[ex.levelsOfSkills[19]] = new uBig(350000)
ex.skillPrices[ex.levelsOfSkills[20]] = new uBig(500000)
ex.skillPrices[ex.levelsOfSkills[21]] = new uBig(1000000)
ex.skillPrices[ex.levelsOfSkills[22]] = new uBig(2000000)
ex.skillPrices[ex.levelsOfSkills[23]] = new uBig(3000000)
ex.skillPrices[ex.levelsOfSkills[24]] = new uBig(4000000)
ex.skillPrices[ex.levelsOfSkills[25]] = new uBig(5000000)

const COMMON_SKILL = 0
const RARE_SKILL = 1
const EMPOWERED_COMMON_SKILL = 2
var dice = 0

ex.Init = function()
{      
   // for (var i = 0; i < ex.NUMBER_OF_SKILLS; i++)
   // {
      // ex.skills.push(new ex.Skill(i))
   // }
   
   // try
   // {   
      // LoadSkillPacks()
   // }
   // catch (err)
   // {
      // CreateSkillPacks()
   // }
}

// ex.CreateSkillPacks = function()
// {
   // uDbg.Log("Can't find skills file, creating a new skills file...")
   // for (var i = 0; i < PRECREATE_LEVELS; i++)
   // {
      // ex.skillPacks[i] = new ex.SkillPack(GetSkillsToPick(3),ex.levelsOfSkills[i])
   // }
   // uMisc.SaveObject(ex.skillPacks,'skillPacks')        
// }

// ex.LoadSkillPacks = function()
// {
   // var dataPacks = uMisc.LoadObjectSync('skillPacks')
   // for (var i = 0; i < PRECREATE_LEVELS; i++)
   // {
      // ex.skillPacks[i] = new ex.SkillPack(null,ex.levelsOfSkills[i])
      // ex.skillPacks[i].CopyFromData(dataPacks[i])
   // }          
// }

ex.GetRandomSkillPack = function(req, game)
{
   return new ex.SkillPack(GetSkillsToPick(3, game), req)
}

ex.SkillPack = function(skills, req, game)
{
   if (skills)
      this.skills = skills
   this.reqMet = false
   this.req = req
   this.picked = -1
   //this.cost = ex.skillPrices[req]
   this.available = false
}

ex.SkillPack.prototype.ResetSkill = function()
{
   this.picked = -1
}

ex.SkillPack.prototype.Copy = function()
{
   var newPack = new ex.SkillPack(this.skills, this.req)
   newPack.picked = this.picked
   newPack.cost = this.cost
   newPack.req = this.req
   newPack.reqMet = this.reqMet
   return newPack
}

// ex.SkillPack.prototype.CopyFromData = function(obj)
// {
   ////var newPack = new ex.SkillPack(this.skills, this.req)
   // this.skills = []
   // for (var i = 0; i < obj.skills.length; i++)
   // {
      // this.skills.push(ex.skills[obj.skills[i].type])
   // }
   // this.reqMet = obj.reqMet
   // this.req = obj.req
   // this.picked = obj.picked
   ////this.cost = obj.cost
// }

ex.SkillPack.prototype.GetPickedSkill = function()
{
   if (this.picked == -1)
      return undefined
   return this.skills[this.picked]
}

ex.SkillPack.prototype.IsSkillPicked = function()
{
   return (this.GetPickedSkill() != undefined)
}

ex.SkillPack.prototype.PickSkill = function(skillType,subType)
{
   uDbg.Log("picking skill, args: skillType =",skillType," subType =",subType)
   uDbg.Log("this.skills.length =",this.skills.length)
   for (var i = 0; i < this.skills.length; i++)
   {
      this.skills[i].available = false
      if (this.skills[i].type == skillType && this.skills[i].subtype == subType)
      {
         uDbg.Log("conditional is true, set this.picked to i")
         this.picked = i
         uDbg.Log("this.picked =",this.picked)
         return
      }
   }
}

function GetEmpowerSkillSubtype(game)
{
   var effectsSum = 0
   var effectsProb = []
   for (var i = 0; i < game.talentEffects.empowerHero.length; i++)
   {
      effectsSum += game.talentEffects.empowerHero[i]+1
      effectsProb.push(effectsSum)
   }
   dice = uMisc.Random(0,effectsSum)
   for (var j = 0; j < uPreferences.NUMBER_OF_HEROES; j++)
   {
      if (dice < effectsProb[j])
         return j      
   }
   return uMisc.Random(0,uPreferences.NUMBER_OF_HEROES)
}

function GetSkillArgs(type,rare)
{
   
}

ex.Skill = function(type,rare,game)
{
   this.type = type
   this.subtype = 0
   this.args = []
   this.rare = rare
   switch (this.type)
   {
      case ex.SKILL_INCREASE_GLOBAL_DAMAGE:         
         this.args.push(4)          //% damage increase
         if (rare === EMPOWERED_COMMON_SKILL)
            this.args[0] = 6
         break
      
      case ex.SKILL_DECREASE_MONSTER_ARMOR:
         this.args.push(20)         //% armor decrease
         if (rare === EMPOWERED_COMMON_SKILL)
            this.args[0] = 30
         break
      
      case ex.SKILL_INCREASE_MONSTER_COINS:
         this.args.push(1)          //number of possible additional coins       
         break
      
      case ex.SKILL_INCREASE_STUN_CHANCE:
         this.args.push(2)          //% chance to stun
         if (rare === EMPOWERED_COMMON_SKILL)
            this.args[0] = 3         
         break
      
      case ex.SKILL_EMPOWER_HERO:
         this.args.push(20)                                            //%
         if (rare === EMPOWERED_COMMON_SKILL)
            this.args[0] = 30
         this.subtype = GetEmpowerSkillSubtype(game)
         //this.subtype = uMisc.Random(0,uPreferences.NUMBER_OF_HEROES)  //empowered hero
         break
      
      case ex.SKILL_HEAL_CHANCE:
         this.args.push(8)          //% chance to heal
         if (rare === EMPOWERED_COMMON_SKILL)
            this.args[0] = 12         
         break
         
      case ex.SKILL_PATHFINDER:
         this.args.push(2)          //% increase rare probability
         this.args.push(0.5)        //% increase elite probability
         break  

      case ex.SKILL_MERCENARY:
         this.args.push(25)        //% rewards increase
         break         
   }   
}

ex.GetAllSkillPacks = function()
{
   var allPacks = []
   for (var i = 0; i < ex.skillPacks.length; i++)
   {
      allPacks.push(ex.skillPacks[i].Copy())
   }
   return allPacks
}

//ex.

// ex.GetSkillPack = function(number,req)
// {
   // return new ex.SkillPack(GetSkillsToPick(number),req)
// }

function GetAnotherEmpowerHero(butNotOfSubType,game,rare)
{   
   var maxHero = -1
   var maxValue = 0
   for (var i = 0; i < game.talentEffects.empowerHero.length; i++)
   {
      if (butNotOfSubType != i && game.talentEffects.empowerHero[i] > maxValue)
      {
         maxValue = game.talentEffects.empowerHero[i]
         maxHero = i
      }
   }
   var skill = new ex.Skill(ex.SKILL_EMPOWER_HERO,rare)
   if (maxHero > -1)
      skill.subtype = maxHero
   while (skill.subtype === butNotOfSubType)
      skill.subtype = uMisc.Random(0,uPreferences.NUMBER_OF_HEROES)
   return skill
}

function GetSkillsToPick(number,game)
{
   var picked = []
   var rare = COMMON_SKILL
   var dice = uMisc.Random(0,100)   
   if (dice < 20)
      rare = RARE_SKILL
   var getAnotherEmpowerHero = false
   var lastEmpowerSubtype = -1
   for (var i = 0; i < number; i++)
   {
      var same = true

      while (same)
      {
         same = false
         var skill = null
         // if (getAnotherEmpowerHero)
         // {
            // skill = GetAnotherEmpowerHero(lastEmpowerSubtype, game, rare)
            // getAnotherEmpowerHero = false
         // }
         // else
            skill = GetRandomSkill(rare,game)         
         for (var j = 0; j < picked.length; j++)
         {
            if (picked[j].type === skill.type && picked[j].subtype === skill.subtype)
            {
               same = true
               break
            }
         }
      }
      if (rare === RARE_SKILL)
         rare = EMPOWERED_COMMON_SKILL
      // if (skill.type === ex.SKILL_EMPOWER_HERO && uMisc.Random(0,100) < 50)
      // {
         // getAnotherEmpowerHero = true
         // lastEmpowerSubtype = skill.subtype
      // }
      picked.push(skill)
   }
   return picked
}

function GetRandomSkill(rare,game)
{
   var skillType = 0
   if (rare === COMMON_SKILL || rare === EMPOWERED_COMMON_SKILL)
   {
      if (uMisc.Random(0,100) < 20)
         skillType = ex.SKILL_EMPOWER_HERO
      else
         skillType = commonSkills[uMisc.Random(0,commonSkills.length)]
   }
   else if (rare === RARE_SKILL)
   {
      skillType = rareSkills[uMisc.Random(0,rareSkills.length)]
   }
   var skill = new ex.Skill(skillType,rare,game)
   return skill
}