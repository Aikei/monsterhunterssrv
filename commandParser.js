var preferences = require('./preferences')
var fs = require('fs')
var uMys = require('./mys.js')
var uSavedData = require('./savedData')

module.exports.Update = function()
{
   //console.log('parser update')
   fs.readFile('cm', function(err, data)
   {
      if (err)
         throw err;
      data = data.toString('utf8')
      //console.log('got data',data)
      var strings = data.split('\n')
      //console.log('got strings',strings)
      for (var i = 0; i < strings.length; i++)
         Parse(strings[i])
      setTimeout(ClearCmdFile, 100)    
   })
}

function ClearCmdFile()
{
   //console.log('in clear cmd file')
   fs.writeFile('cm','',function (err, data) 
   {      
      if (err)
         throw err
      //console.log('successfully cleared cm')
   })     
}

function Parse(cmd)
{
   //console.log('parsing cmd "'+cmd+'"')
   cmd = cmd.trim()
   //console.log('trimmed cmd: "'+cmd+'"')
   if (cmd === 'st')
   {
      console.log('shutting down...')
      preferences.shuttingDown = true
      setTimeout(uMys.CloseServer, 3000)
   }
   else if (cmd === 'new_day')
   {
      console.log('starting new day...')
      uMys.StartNewDay()
   }
   else if (cmd.charAt(0) === 'e')
   {
      cmd = cmd.slice(2)
      uMys.Eval(cmd)
   }
   else if (cmd.charAt(0) === 'c' && cmd.charAt(1) === 'h' && cmd.charAt(2) === ' ')
   {
      cmd = cmd.slice(3)
      cmd = cmd.split(',')
      for (var i = 0; i < cmd.length; i++)
         uSavedData.cheaters.push(cmd[i])
   }
}