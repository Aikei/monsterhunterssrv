var uDungeon = require('./dungeon')
var uWeapons = require('./weapons')
var uMonsters = require('./monsters')
var uHeroes = require('./heroes')
var uPreferences = require('./preferences')
var uMessage = require('./message')
var uBig = require('big.js')
var uMisc = require('./misc')
var uSkills = require('./skills')
var uDbg = require('./debug')
var uStats = require('./stats')
var uBigs = require('./bigInts')
var uLeaderboard = require('./leaderboard')
var uGameProfile = require('./profile/gameProfile.js')
var uQuest = require('./quest')
var uBalance = require('./balanceData')

var ex = module.exports
var sDice = 0

ex.Game = function(id,data)
{
   if (data == undefined)
   {
      this.currentWeapon = uWeapons.GetWeapon(uPreferences.WEAPON_WOODEN_SWORD,1)
      this.currentHeroes = []
      this.coins = new uBig(0)
      this.glory = new uBig(0)      
      this.nextAvailableSkillPackNumber = 0
      this.skills = []
      this.skillsReqs = uSkills.levelsOfSkills
      //this.skills = uSkills.GetAllSkillPacks()
      this.talentEffects = 
      { 
         heroDpsIncrease: 0, 
         armorDecrease: 0, 
         coinsIncrease: 0, 
         addCoinChance: 10, 
         overallGoldIncrease: 0, 
         stunDuration: 2, 
         stunChance: 1, 
         empowerHero: [0, 0, 0, 0, 0, 0], 
         poisonDurationDecrease: 0, 
         healChance: 0, 
         rareChanceIncrease: 0,
         eliteChanceIncrease: 0,
         questRewardsIncrease: 0
      }
      this.reseted = false
      this.maxDungeonLevel = 1
      this.stats = new uStats()
      this.id = id
      this.groupName = "Default"+uGameProfile._profiles[this.id].sqid
	   this.lastShare = 0
      this.tut = true
      // this.monsterArmorProbability = []
      // for (var i = 1; i < uPreferences.ARMOR_LAST; i++)
      // {
      //    this.monsterArmorProbability.push(uPreferences.BASE_ARMOR_PROBABILITY)
      // }
      this.bgp = null
      this.dungeonShellsList = []
      this.currentDungeon = 0  
      for (i = 0; i < uPreferences.NUMBER_OF_DUNGEONS; i++)
      {
         this.dungeonShellsList.push(new uDungeon.DungeonShell(i))
         if (i === this.currentDungeon)
         {
            this.dungeonShellsList[i].current = true
         }
      }
      this.q = null
      this.PLog("Game created")
   }
   else
   {
      for (var key in data)
      {
         this[key] = data[key]
      }     
      this.PLog("Connected to existing game")
      //big
      this.glory = new uBig(data.glory)
      this.coins = new uBig(data.coins)
      this.talentEffects.armorDecrease = Number(data.talentEffects.armorDecrease)
      
      //heroes
      this.currentHeroes = []
      for (var i = 0; i < data.currentHeroes.length; i++)
      {
         this.currentHeroes.push(new uHeroes.ActualHero(data.currentHeroes[i].heroData, data.currentHeroes[i].id))
         this.currentHeroes[i].heroData.dps.value = new uBig(data.currentHeroes[i].heroData.dps.value)
      }
      
      //skills
      this.skills = []
      for (var i = 0; i < data.skills.length; i++)
      {
         var dataSkillPack = data.skills[i]
         var pack = new uSkills.SkillPack(null,dataSkillPack.req,this)
         pack.skills = []
         for (var j = 0; j < data.skills[i].skills.length; j++)
         {
            pack.skills[j] = data.skills[i].skills[j]
         }
         pack.num = i
         pack.picked = dataSkillPack.picked
         pack.available = dataSkillPack.available
         this.skills[i] = pack
      }
      if (data.q != null && data.q.ac)
      {
         this.q = new uQuest.Quest(data.q)
      }
      else
         this.q = null
      
      //reapply skills
      //this.ReapplySkills() 
      
      this.reseted = false
      //reset skills
      // if (!this.reseted)
      // {
         // this.ResetSkills()
         // this.reseted = true
      // }
      
      //stats
      this.stats = new uStats(data.stats)
      this.goldLimit = new uBig(data.goldLimit)
      this.bgp = this.BackgroundProgress()
   }
   this.rareLoaded = false
   this.dungeonShellsList[0].current = false
   this.dungeonShellsList[this.currentDungeon].current = true
   this.CalculateGoldLimit()
   uDungeon.__dungeons[this.dungeonShellsList[this.currentDungeon].type].playersInDungeon++
   uLeaderboard.MakeEntry(this.id,this.groupName)
}

ex.Game.prototype.PLog = function()
{
   var allArgs = [this.id]
   for (var i = 0; i < arguments.length; i++)
      allArgs.push(arguments[i])
   uDbg.LogToPlayerLog.apply(null,allArgs)
}

ex.Game.prototype.CalculateGoldLimit = function()
{
   this.goldLimit = uBalance.GetGoldLimit(this.dungeonShellsList[this.currentDungeon].level,this.stats.totalGoldCollected)
}

ex.Game.prototype.SendQuestProgress = function(socket)
{
   uMessage.SendMessage(socket, new uMessage.GameMessage(uMessage.Names.ksQuestProgress, this.q.p))
}

ex.Game.prototype.GetCurrentDungeon = function()
{
   return uDungeon.__dungeons[this.dungeonShellsList[this.currentDungeon].type]
}

ex.Game.prototype.GetCurrentDungeonShell = function()
{
   return this.dungeonShellsList[this.currentDungeon]
}

ex.Game.prototype.GetHpShare = function(level)
{
   return uMonsters.GetHpShare(level,this.maxDungeonLevel)
}

ex.Game.prototype.BackgroundProgress = function()
{
   var obj = { gold: uBigs.ZERO, glory: uBigs.ZERO, killed: uBigs.ZERO }
   return obj
}

/*
ex.Game.prototype.BackgroundProgress = function()
{
   var obj = { gold: uBigs.ZERO, glory: uBigs.ZERO, killed: uBigs.ZERO }
   var timePassed = new uBig(uMisc.GetServerTime() - uGameProfile._profiles[this.id].quitTime)
   if (timePassed.gt(uPreferences.MAX_TIME_OFFLINE_BIG))
   {
      timePassed = uPreferences.MAX_TIME_OFFLINE_BIG
   }
   if (timePassed.lt(uBigs.SIXTY) || this.currentHeroes.length == 0)
      return null
   var heroesDps = this.CalculateTotalHeroesDps()      
   var monsterHp = uBigs.ZERO
   var rareHp = uBigs.ZERO
   var eliteHp = uBigs.ZERO
   var killTimes = {}
   killTimes.commonKillTime = uBigs.ZERO
   killTimes.rareKillTime = uBigs.ZERO
   killTimes.eliteKillTime = uBigs.ZERO
   var thisDungShell = this.GetCurrentDungeonShell()
   for (var i = 0; i < 2; i++)
   {
      monsterHp = uMonsters.RetrieveMonsterHealth(thisDungShell.level)
      // rareHp = monsterHp.times(uPreferences.RARE_HP_MULTIPLIER)
      // eliteHp = monsterHp.times(uPreferences.ELITE_HP_MULTIPLIER)
      rareHp = monsterHp.times(uBalance.rare.rareHpMultiplier)
      eliteHp = monsterHp.times(uBalance.rare.eliteHpMultiplier)      
      killTimes.commonKillTime = monsterHp.div(heroesDps)
      killTimes.rareKillTime = rareHp.div(heroesDps)
      killTimes.eliteKillTime = eliteHp.div(heroesDps)
      if (killTimes.commonKillTime.gt(uBigs.THIRTY) || killTimes.rareKillTime.gt(uBigs.THIRTY))
      {
         if (thisDungShell.maxLevel == thisDungShell.level)
         {
            thisDungShell.level = thisDungShell.maxLevel-1
            this.CalculateGoldLimit()
         }
         else
            break
      }
      else
         break
   } 

   // killTimes.commonKillTime = killTimes.commonKillTime.plus(uBigs.ONE)
   // killTimes.rareKillTime = killTimes.rareKillTime.plus(uBigs.ONE)
   // killTimes.eliteKillTime = killTimes.eliteKillTime.plus(uBigs.ONE)
   // var chances = uMonsters.GetRareChances(thisDungShell.level,this.maxDungeonLevel,this.talentEffects)
   // var goldPerMonster = uMonsters.RetrieveMonsterGold(thisDungShell.level)
   // var killed = CalculateKilled(timePassed,chances,killTimes)   
   
   
   // obj.gold = killed.commonKilled.times(goldPerMonster).plus(killed.rareKilled.times(goldPerMonster.times(uBigs.TWO))).plus(killed.eliteKilled.times(goldPerMonster.times(uBigs.THREE))).div(uBigs.TEN).round()
   // obj.glory = killed.rareKilled.times(uPreferences.RARE_MONSTER_GLORY).plus(killed.eliteKilled.times(uPreferences.ELITE_MONSTER_GLORY)).div(uBigs.TEN).round()
   // obj.killed = killed.commonKilled.plus(killed.rareKilled).plus(killed.eliteKilled).round()
   
   var refillSpeed = uBalance.GetGoldRefillSpeed(thisDungShell.maxLevel)
   obj.gold = timePassed.times(refillSpeed).round()
   if (obj.gold.gt(this.goldLimit))
   {
      obj.gold = this.goldLimit
   }
   
   this.PLog("Background progress - gold:",obj.gold,"at level: ",thisDungShell.level,"max level of this dungeon: ",thisDungShell.maxLevel,"overall max level: ",this.maxDungeonLevel)   

   // this.PLog("Background progress - gold:",obj.gold,"glory: ",obj.glory,"killed: ",obj.killed,"at level: ",thisDungShell.level,
               // "max level of this dungeon: ",thisDungShell.maxLevel,"overall max level: ",this.maxDungeonLevel)
   return obj
}
*/

// ex.Game.prototype.BackgroundProgress = function()
// {
   // var obj = { gold: uBigs.ZERO, glory: uBigs.ZERO, killed: uBigs.ZERO }
   // var timePassed = new uBig(uMisc.GetServerTime() - uGameProfile._profiles[this.id].quitTime)
   // if (timePassed.gt(uPreferences.MAX_TIME_OFFLINE_BIG))
   // {
      // timePassed = uPreferences.MAX_TIME_OFFLINE_BIG
   // }
   //console.log('time passed', timePassed)
   // if (timePassed.lt(uBigs.SIXTY) || this.currentHeroes.length == 0)
      // return null
   // var heroesDps = this.CalculateTotalHeroesDps()      
   // var monsterHp = uBigs.ZERO
   // var rareHp = uBigs.ZERO
   // var eliteHp = uBigs.ZERO
   // var killTimes = {}
   // killTimes.commonKillTime = uBigs.ZERO
   // killTimes.rareKillTime = uBigs.ZERO
   // killTimes.eliteKillTime = uBigs.ZERO
   // thisDungShell.level++
   // var thisDungShell = this.GetCurrentDungeonShell()
   // for (var i = 0; i < 2; i++)
   // {
      // thisDungShell.level--
      // monsterHp = uMonsters.RetrieveMonsterHealth(thisDungShell.level)
      // rareHp = monsterHp.times(uPreferences.RARE_HP_MULTIPLIER)
      // eliteHp = monsterHp.times(uPreferences.ELITE_HP_MULTIPLIER)
      // killTimes.commonKillTime = monsterHp.div(heroesDps)
      // killTimes.rareKillTime = rareHp.div(heroesDps)
      // killTimes.eliteKillTime = eliteHp.div(heroesDps)
      // if (killTimes.commonKillTime.gt(uBigs.THIRTY) || killTimes.rareKillTime.gt(uBigs.THIRTY))
      // {
         // if (thisDungShell.maxLevel == thisDungShell.level)
         // {
            // thisDungShell.level = thisDungShell.maxLevel-1
            // this.CalculateGoldLimit()
         // }
         // else
            // break
      // }
      // else
         // break
   // } ////while (killTimes.commonKillTime.gt(uBigs.THIRTY) || killTimes.rareKillTime.gt(uBigs.THIRTY))

   // killTimes.commonKillTime = killTimes.commonKillTime.plus(uBigs.ONE)
   // killTimes.rareKillTime = killTimes.rareKillTime.plus(uBigs.ONE)
   // killTimes.eliteKillTime = killTimes.eliteKillTime.plus(uBigs.ONE)
   // var chances = uMonsters.GetRareChances(thisDungShell.level,this.maxDungeonLevel,this.talentEffects)
   // var goldPerMonster = uMonsters.RetrieveMonsterGold(thisDungShell.level)
   // var killed = CalculateKilled(timePassed,chances,killTimes)   
   // var rareKilled = killed.times(new uBig(chances.rare/uPreferences.PROBABILITY_WIDTH)).round(0,0)
   // var eliteKilled = killed.times(new uBig(chances.elite/uPreferences.PROBABILITY_WIDTH)).round(0,0)
   // var commonKilled = killed.minus(rareKilled).minus(eliteKilled)
   
   // obj.gold = killed.commonKilled.times(goldPerMonster).plus(killed.rareKilled.times(goldPerMonster.times(uBigs.TWO))).plus(killed.eliteKilled.times(goldPerMonster.times(uBigs.THREE))).div(uBigs.TEN).round()
   // obj.glory = killed.rareKilled.times(uPreferences.RARE_MONSTER_GLORY).plus(killed.eliteKilled.times(uPreferences.ELITE_MONSTER_GLORY)).div(uBigs.TEN).round()
   // obj.killed = killed.commonKilled.plus(killed.rareKilled).plus(killed.eliteKilled).round()
      
   // if (obj.gold.gt(this.goldLimit))
   // {
      // var limitShare = this.goldLimit.div(obj.gold)
      // obj.glory = obj.glory.times(limitShare)
      // obj.gold = this.goldLimit
   // }

   //this.coins = this.coins.plus(obj.gold)
   // this.PLog("Background progress - gold:",obj.gold,"glory: ",obj.glory,"killed: ",obj.killed,"at level: ",thisDungShell.level,
               // "max level of this dungeon: ",thisDungShell.maxLevel,"overall max level: ",this.maxDungeonLevel)
   // return obj
// }

function CalculateKilled(time,chances,killTimes)
{
   var rareShare = new uBig(chances.rare/uPreferences.PROBABILITY_WIDTH)
   var eliteShare = new uBig(chances.elite/uPreferences.PROBABILITY_WIDTH)
   var rareTime = time.times(rareShare)
   var eliteTime = time.times(eliteShare)
   var commonTime = time.minus(rareTime).minus(eliteTime)
   var obj = {}
   obj.commonKilled = commonTime.div(killTimes.commonKillTime)
   if (killTimes.rareKillTime.lte(uBigs.THIRTY_TWO))
      obj.rareKilled = rareTime.div(killTimes.rareKillTime)
   else
      obj.rareKilled = uBigs.ZERO
   if (killTimes.eliteKillTime.lte(uBigs.THIRTY_TWO))
      obj.eliteKilled = eliteTime.div(killTimes.eliteKillTime)
   else
      obj.eliteKilled = uBigs.ZERO
   return obj
}

ex.Game.prototype.CalculateTotalHeroesDps = function()
{
   var armorShares = this.GetCurrentDungeon().GetArmorTypesByShare()
   var total = uBigs.ZERO
   for (var i = 0; i < this.currentHeroes.length; i++)
   {
      var value = this.currentHeroes[i].heroData.dps.value
      if (armorShares[this.currentHeroes[i].heroData.dps.type] > 0)
      {
         value = value.times(new uBig(armorShares[this.currentHeroes[i].heroData.dps.type]+1))
      }
      total = total.plus(value)
   }
   return total
}

ex.Game.prototype.GetLowestLevelDungeonType = function()
{
   var lowestLevel = this.dungeonShellsList[0].level
   var lowestType = this.dungeonShellsList[0].type
   for (var i = 0; i < this.dungeonShellsList.length; i++)
   {
      if (this.dungeonShellsList[i].level < lowestLevel)
      {
         lowestLevel = this.dungeonShellsList[i].level
         lowestType = this.dungeonShellsList[i].type
      }
   }
   return lowestType
}

ex.Game.prototype.ChangeGroupName = function(newName,socket)
{  
   var result = uLeaderboard.ChangeGroupName(this.id,newName)
   if (result)
      this.groupName = newName
   uMessage.SendMessage(socket, new uMessage.GameMessage(uMessage.Names.ksRenameResponse, result))
}

ex.Game.prototype.OnPlayerLeave = function()
{
   uDungeon.__dungeons[this.dungeonShellsList[this.currentDungeon].type].playersInDungeon--
   for (var i = 0; i < this.currentHeroes.length; i++)
   {
      this.currentHeroes[i].ClearAfflictions()
   }
}

ex.Game.prototype.CreateNewMonster = function(socket,noRare)
{
   this.currentMonster = uMonsters.GetMonster(this,noRare)
   if (this.q)
   {
      this.q.OnNewMonster(this)
   }
   else if (uMisc.Random(0,100) < uPreferences.QUEST_PROBABILITY)
   {
      this.currentMonster.q = uQuest.GetRandomQuest(this)
   }
   
   //this.currentMonster.armorType = this.GetArmorType()
   // if (this.talentEffects.coinsIncrease > 0)
   // {
      // var newGold = this.currentMonster.monsterData.gold.plus(this.currentMonster.monsterData.gold.div(uMisc.BIG_ONE_HUNDRED).times(new uBig(this.talentEffects.coinsIncrease))).round(0,2)
      // this.currentMonster.coinsData = this.currentMonster.GetCoinsData(newGold)
   // }   
}

ex.Game.prototype.GetHero = function(type)
{
   for (var i = 0; i < this.currentHeroes.length; i++)
   {
      if (this.currentHeroes[i].type === type)
         return this.currentHeroes[i]
   }
   return null
}

// ex.Game.prototype.GetArmorType = function()
// {
   // sDice = uMisc.Random(0, uPreferences.PROBABILITY_WIDTH);
   // var i = 0
   // for (; i < this.monsterArmorProbability.length; i++)
   // {
      // if (sDice < this.monsterArmorProbability[i]*(i+1))
      // {
         // break;
      // }
   // }
   // return i+1
// }

ex.Game.prototype.StartNextLevel = function(socket)
{
   var dung = this.dungeonShellsList[this.currentDungeon]
   dung.level++
   if (dung.level > this.maxDungeonLevel)
      this.maxDungeonLevel = dung.level
   if (dung.level > dung.maxLevel)
      dung.maxLevel = dung.level
   dung.monstersKilled = 0
   this.CalculateGoldLimit()
   this.CreateNewMonster(socket,true)
   
   var msg = new uMessage.GameMessage(uMessage.Names.ksNextLevel, 
            { 
                  m: this.currentMonster, 
                  l: dung.level, 
                  gl: this.goldLimit,
                  dd: { sh: this.dungeonShellsList, d: uDungeon.__dungeons }
            } )
   uMessage.SendMessage(socket, msg)   
}

// ex.Game.prototype.MaybeSendNewSkillPack = function(socket)
// {
   // var req = uSkills.levelsOfSkills[this.lastSkillPackNumber]
   // if (this.glory.gte(new uBig(req)))
   // {
      // var pack = new uSkills.GetSkillPack(3,req)
      // pack.num = this.skills.length
      // this.skills.push(pack)
      // this.lastSkillPackNumber++
      // var message = new uMessage.Message(uMessage.Names.kGameMessage, uMessage.Names.ksNewSkillPack, pack)
      // uMessage.SendMessage(socket,message)
   // }
// }

ex.Game.prototype.MaybeSendNewSkillPack = function(socket)
{
   if (this.nextAvailableSkillPackNumber >= uPreferences.NUMBER_OF_SKILL_PACKS)
      return
   var req = uSkills.levelsOfSkills[this.nextAvailableSkillPackNumber]
   var newSkillPacks = []
   while (req && this.glory.gte(new uBig(req)))
   {
      var skillPack = uSkills.GetRandomSkillPack(req,this)
      skillPack.num = this.skills.length
      skillPack.available = true
      newSkillPacks.push(skillPack)
      this.skills.push(skillPack)
      //console.log('this.skills:\n',this.skills)
      //var message = new uMessage.Message(uMessage.Names.kGameMessage, uMessage.Names.ksNextSkillPackAvailable, { req: req, num: this.nextAvailableSkillPackNumber } )      
      this.nextAvailableSkillPackNumber++
      req = uSkills.levelsOfSkills[this.nextAvailableSkillPackNumber]      
   }
   var message = new uMessage.Message(uMessage.Names.kGameMessage, uMessage.Names.ksNewSkillPack, newSkillPacks)
   uMessage.SendMessage(socket,message)
}

ex.Game.prototype.CreateMonsterAndSendMessage = function(socket,noRare)
{
   this.CreateNewMonster(socket,noRare)
   var msg = new uMessage.GameMessage(uMessage.Names.ksNewMonster, 
      { 
         m: this.currentMonster, 
         kl: this.dungeonShellsList[this.currentDungeon].monstersKilled, 
         dd: { sh: this.dungeonShellsList, d: uDungeon.__dungeons } 
      } )
   uMessage.SendMessage(socket,msg)
}

ex.Game.prototype.AddGlory = function(value,socket)
{
   this.glory = this.glory.plus(value)
   uLeaderboard.ChangeGlory(this.id,this.glory,value)
   if (socket)
      this.MaybeSendNewSkillPack(socket)
}

ex.Game.prototype.ProcessFirstConnectMessage = function(message,socket)
{
   // if (this.bgp)
   // {
      // this.AddGlory(this.bgp.glory, socket)
   // }
}

ex.Game.prototype.ResetSkills = function()
{
   this.RenewSkills()
   this.ResetTalentEffects()
   for (var i = 0; i < this.skills.length; i++)
   {
      this.skills[i].ResetSkill()
   }
}

ex.Game.prototype.ResetTalentEffects = function()
{
   this.talentEffects = 
   { 
      heroDpsIncrease: 0, 
      armorDecrease: 0, 
      coinsIncrease: 0, 
      addCoinChance: 10, 
      overallGoldIncrease: 0, 
      stunDuration: 2, 
      stunChance: 1, 
      empowerHero: [0, 0, 0, 0, 0, 0], 
      poisonDurationDecrease: 0, 
      healChance: 0, 
      rareChanceIncrease: 0,
      eliteChanceIncrease: 0,
      questRewardsIncrease: 0
   }   
}

ex.Game.prototype.ApplySkill = function(skill)
{
   switch (skill.type)
   {
      case uSkills.SKILL_INCREASE_GLOBAL_DAMAGE:
         this.talentEffects.heroDpsIncrease += skill.args[0]
         break
      case uSkills.SKILL_DECREASE_MONSTER_ARMOR:
         this.talentEffects.armorDecrease += skill.args[0]-((skill.args[0]*this.talentEffects.armorDecrease) / 100)
         break
      case uSkills.SKILL_INCREASE_MONSTER_COINS:
         this.talentEffects.coinsIncrease += skill.args[0]
         break
      case uSkills.SKILL_INCREASE_STUN_CHANCE:
         this.talentEffects.stunChance += skill.args[0]
         break
      case uSkills.SKILL_EMPOWER_HERO:
         this.talentEffects.empowerHero[skill.subtype] += skill.args[0]
         break
      case uSkills.SKILL_HEAL_CHANCE:
         this.talentEffects.healChance += skill.args[0]
         break
      case uSkills.SKILL_PATHFINDER:
         this.talentEffects.rareChanceIncrease += skill.args[0]
         this.talentEffects.eliteChanceIncrease += skill.args[1]
         break
      case uSkills.SKILL_MERCENARY:
         this.talentEffects.questRewardsIncrease += skill.args[0]
         break              
   }   
}

ex.Game.prototype.RenewSkills = function()
{
   for (var i = 0; i < this.skills.length; i++)
   {
      var rarePack = false
      for (var j = 0; j < this.skills[i].skills.length; j++)
      {
         if (this.skills[i].skills[j].type === uSkills.SKILL_MERCENARY || this.skills[i].skills[j].type === uSkills.SKILL_PATHFINDER)
         {
            rarePack = true
            break
         }           
      }
      for (var j = 0; j < this.skills[i].skills.length; j++)
      {      
         var newSkill = new uSkills.Skill(this.skills[i].skills[j].type, rarePack, this)
         this.skills[i].skills[j].args = newSkill.args
      }
   }
}

ex.Game.prototype.ReapplySkills = function()
{
   this.RenewSkills()
   this.ResetTalentEffects()
   for (var i = 0; i < this.skills.length; i++)
   {
      var pickedSkill = this.skills[i].GetPickedSkill()
      if (pickedSkill)
         this.ApplySkill(pickedSkill)
   }
}

ex.Game.prototype.Cheater = function(socket,str)
{
   this.PLog("thought to be cheater, reason: '",str,"'")
   uMessage.SendCheater(socket,str)
}

ex.Game.prototype.ProcessGameMessage = function(message,socket)
{
   if (message.s === uMessage.Names.ksMonsterKilled)
   {
      //check
      if (this.q)
      {
         if (this.q.OnMonsterKilled(this.currentMonster,this))
         {
            this.SendQuestProgress(socket)
            if (this.q.p === -1)
               this.q = null            
         }
      }
      else if (this.currentMonster.q != null)
      {
         this.q = this.currentMonster.q
      }
      this.stats.monstersKilled = this.stats.monstersKilled.plus(uBigs.ONE)
      if (this.currentMonster.glory.gt(uBigs.ZERO))
      {
         if (this.currentMonster.rarity == uPreferences.RARITY_RARE)
            this.stats.rareMonstersKilled = this.stats.rareMonstersKilled.plus(uBigs.ONE)
         else if (this.currentMonster.rarity == uPreferences.RARITY_ELITE)
            this.stats.eliteMonstersKilled = this.stats.eliteMonstersKilled.plus(uBigs.ONE)
         //this.AddGlory(this.currentMonster.glory)        
      }
      if (this.dungeonShellsList[this.currentDungeon].maxLevel == this.dungeonShellsList[this.currentDungeon].level)
      {
         this.dungeonShellsList[this.currentDungeon].monstersKilled++
         if (this.dungeonShellsList[this.currentDungeon].monstersKilled === uPreferences.MONSTERS_PER_LEVEL)
         {
            setTimeout(this.StartNextLevel.bind(this), 1500, socket)
         }
         else
         {
            setTimeout(this.CreateMonsterAndSendMessage.bind(this), 1000, socket)
         }         
      }
      else
      {
         setTimeout(this.CreateMonsterAndSendMessage.bind(this), 1000, socket)
      }
   }
   else if (message.s === uMessage.Names.ksCoinsReceived)
   {
      //check
      var plusGold = new uBig(message.m_data)
      if (this.goldLimit.gt(uBigs.ZERO))
      {
         this.goldLimit = this.goldLimit.minus(plusGold)
         if (this.goldLimit.lt(uBigs.ZERO))
            this.goldLimit = uBigs.ZERO
      }
      this.coins = this.coins.plus(plusGold)
      this.stats.totalGoldCollected = this.stats.totalGoldCollected.plus(plusGold)
   }
   else if (message.s === uMessage.Names.ksGloryReceived)
   {
      //check
      var plusGlory = new uBig(message.m_data)
      this.AddGlory(plusGlory,socket)
   }   
   else if (message.s === uMessage.Names.ksWeaponUpgraded)
   {
      //check
      if (this.coins.gte(this.currentWeapon.levelUpCost))
      {
         this.coins = this.coins.minus(this.currentWeapon.levelUpCost)
         this.currentWeapon = uWeapons.GetWeapon(this.currentWeapon.type,this.currentWeapon.level+1)
         message.m_data = this.currentWeapon
         uMessage.SendMessage(socket,message)
      }
      else
      {
         var str = "when upgrading weapon - coins: "+this.coins.toString()+", "+"level up cost: "+this.currentWeapon.levelUpCost.toString()
         this.Cheater(socket,str)
      }   
   }
   else if (message.s === uMessage.Names.ksChangeDungeon)
   {
      uDungeon.__dungeons[this.dungeonShellsList[this.currentDungeon].type].playersInDungeon--
      this.dungeonShellsList[this.currentDungeon].current = false
      this.currentDungeon = message.m_data
      uDungeon.__dungeons[this.dungeonShellsList[this.currentDungeon].type].playersInDungeon++
      this.CalculateGoldLimit()
      this.CreateNewMonster(socket,true)
      message.m_data = 
      { 
         cd: this.currentDungeon, 
         m: this.currentMonster,
         gl: this.goldLimit,
         dd: { sh: this.dungeonShellsList, d: uDungeon.__dungeons } 
      }
      uMessage.SendMessage(socket,message)
   }
   else if (message.s === uMessage.Names.ksBuyHero)
   {
      this.stats.totalHeroesLevel = this.stats.totalHeroesLevel.plus(uBigs.ONE)
      var same = false
      for (var i = 0; i < this.currentHeroes.length; i++)
      {
         if (this.currentHeroes[i].type == message.m_data)
         {
            var str = "when buying hero - trying to buy already existing hero"
            this.Cheater(socket,str)
            return
         }
      }
      var heroData = uHeroes.GetHeroData(message.m_data,1)
      if (this.coins.gte(heroData.buyCost))
      {
         this.coins = this.coins.minus(heroData.buyCost)
         var h = new uHeroes.ActualHero(heroData, this.currentHeroes.length)
         this.currentHeroes.push(h)
         message.m_data = h
         uMessage.SendMessage(socket,message)
      }
      else
      {
         str = "when buying hero - coins: "+this.coins.toString()+", buy cost: "+heroData.buyCost.toString()
         this.Cheater(socket,str)
      }
   }
   else if (message.s === uMessage.Names.ksUpgradeHero)
   {
      this.stats.totalHeroesLevel = this.stats.totalHeroesLevel.plus(uBigs.ONE)
      var ah = this.currentHeroes[message.m_data]
      //var heroData = uHeroes.GetHeroDataByActualHero(ah)
      if (this.coins.gte(ah.heroData.levelUpCost))
      {
         this.coins = this.coins.minus(ah.heroData.levelUpCost)      
         //ah.level++
         //heroData = uHeroes.GetHeroDataByActualHero(ah)
         ah.IncreaseLevel(1)
         message.m_data = { hd: ah.heroData, ah: ah }
         uMessage.SendMessage(socket,message)
      }
      else
      {
         var str = "when upgrading hero - coins: "+this.coins.toString()+", "+"level up cost: "+ah.heroData.levelUpCost.toString()
         this.Cheater(socket,str)
      }      
   }
   else if (message.s === uMessage.Names.ksWeaponBought)
   {
      var w = uWeapons.GetWeapon(message.m_data,1) 
      if (this.coins.gte(w.buyCost))
      {      
         this.currentWeapon = w
         message.m_data = w
         uMessage.SendMessage(socket,message)
      }
      else
      {
         var str = "coins: "+this.coins.toString()+", "+"buy cost: "+w.buyCost.toString()
         this.Cheater(socket,str)
      }
   }
   else if (message.s === uMessage.Names.ksSkillPicked)
   {
      uDbg.Log('skill picked message:',message);
      this.stats.totalSkillsPicked = this.stats.totalSkillsPicked.plus(uBigs.ONE)
      var skillPack = this.skills[message.m_data.packNum]
      uDbg.Log('skillPack:',skillPack)
      if (!skillPack.IsSkillPicked())
      {        
         skillPack.PickSkill(message.m_data.skillType, message.m_data.subType)
         uDbg.Log('skillPack after picking:',skillPack)
         var skill = skillPack.GetPickedSkill()
         this.ApplySkill(skill)
         //uDbg.Log("_ii","picked skill", this.skills[message.m_data.req])
      }
      else
      {        
         this.Cheater(socket,"wrong skill")
      }      
   }
   else if (message.s === uMessage.Names.ksMonsterRan)
   {
      this.stats.monstersRan = this.stats.monstersRan.plus(uBigs.ONE)
      setTimeout(this.CreateMonsterAndSendMessage.bind(this), 1000, socket, true)
   }
   else if (message.s === uMessage.Names.ksStartGame)
   {
      this.CreateMonsterAndSendMessage(socket,true)
   }   
   else if (message.s === uMessage.Names.ksAfflictionAdded)
   {
      this.stats.afflictionsSuffered = this.stats.afflictionsSuffered.plus(uBigs.ONE)
      var h = this.GetHero(message.m_data.heroType)
      if (h != null)
      {
         if (h.HasAffliction(message.m_data.afflictionType))
            h.RemoveAffliction(message.m_data.afflictionType)
         h.AddAffliction(message.m_data.afflictionType)
      }
      else
      {
         this.Cheater(socket,"can't find hero of type:",message.m_data.heroType)
      }
   }
   else if (message.s === uMessage.Names.ksAfflictionRemoved)
   {
      var h = this.GetHero(message.m_data.heroType)
      if (h != null)
      {
         h.RemoveAffliction(message.m_data.afflictionType)
      }
      else
      {
         this.Cheater(socket,"can't find hero of type:",message.m_data.heroType)
      }
   }
   else if (message.s === uMessage.Names.ksRequestLeaderboard)
   {
      message.m_data = uLeaderboard.GetDeflatedLeaderboard(this.id)
      uMessage.SendMessage(socket,message)
   }
   else if (message.s === uMessage.Names.ksSquadNameChanged)
   {
      this.ChangeGroupName(message.m_data,socket)
   }
   else if (message.s === uMessage.Names.ksBgReportShared)
   {
      this.lastShare = message.m_data
   }
   else if (message.s === uMessage.Names.ksTutorialDone)
   {
      this.tut = false
   }
   else if (message.s === uMessage.Names.ksQuestAccepted)
   {
      this.q.ac = true
      if (this.q.t === uQuest.QUEST_TYPE_KILL_BOSS)
      {
         this.q.dt.bph = {}
         this.q.dt.bph.h = message.m_data.h
         this.q.dt.bph.d = message.m_data.d
      }
   }
   else if (message.s === uMessage.Names.ksQuestRefused)
   {
      this.q = null
   }
   else if (message.s === uMessage.Names.ksRareLoaded)
   {
      this.rareLoaded = true
   }
   else if (message.s === uMessage.Names.ksChangeLevel)
   {
      this.dungeonShellsList[this.currentDungeon].level = message.m_data.l
      this.CalculateGoldLimit()
      this.CreateNewMonster(socket,true)
      message.m_data.gl = this.goldLimit
      message.m_data.m = this.currentMonster
      message.m_data.dd = { sh: this.dungeonShellsList, d: uDungeon.__dungeons }
      if (this.dungeonShellsList[this.currentDungeon].level < this.dungeonShellsList[this.currentDungeon].maxLevel)
      {
         message.m_data.lst = false
         message.m_data.mk = 10
      }
      else
      {
         message.m_data.lst = true
         message.m_data.mk = this.dungeonShellsList[this.currentDungeon].monstersKilled
      }
      uMessage.SendMessage(socket,message)
   }     
}
