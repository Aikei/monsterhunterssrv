var uBig = require('big.js')
var uBigs = require('./bigInts.js')
var uPreferences = require('./preferences')
var uSavedData = require('./savedData')
var uFs = require('fs')
var uHeroes = require('./heroes')
var uMonsters = require('./monsters')

var ex = module.exports

ex.PRECREATE_MONSTER_LEVELS = 100
ex.PRECREATE_HERO_LEVELS = 100
ex.PRECREATE_GOLD_LIMITS = 100
ex.DAMAGE_COST = new uBig(10)

var baseRefillSpeed = uBigs.ZERO

//ex.data = {}
ex.jsonData = {}

uBig.RM = 0

var difficulties = [ 2, 3, 3, 3, 3, 4, 4, 4, 6, 6, 8, 8, 10, 10, 14, 14, 18, 22, 26, 30, 60 ]

const ARMOR_DIVIDER = new uBig(7*20)
const coinIncreaseIncrement = new uBig(1.1)
const coinIncreaseBase = new uBig(10)


ex.Init = function()
{
   // try
   // {
      // var f = uFs.readFileSync('data/balanceData', { encoding : 'utf8' } )
      // ex.data = JSON.parse(f)
      
      f = uFs.readFileSync('data/data.js', { encoding : 'utf8' } )
      ex.jsonData = JSON.parse(f)
      
      for (var key in ex.jsonData.heroes)
      {
         if (ex.jsonData.heroes[key].cost != undefined)
         {
            ex.jsonData.heroes[key].cost = new uBig(ex.jsonData.heroes[key].cost)
            ex.jsonData.heroes[key].dps = new uBig(ex.jsonData.heroes[key].cost)
         }
      }
      
      ex.jsonData.monsters.baseGold = new uBig(ex.jsonData.monsters.baseGold)
      ex.jsonData.monsters.baseHp = new uBig(ex.jsonData.monsters.baseHp)
      
       for (var key in ex.jsonData.monsters)
      {
         if (ex.jsonData.monsters[key].goldMult != undefined)
         {
            ex.jsonData.monsters[key].goldMult = new uBig(ex.jsonData.monsters[key].goldMult)
            ex.jsonData.monsters[key].hpMult = new uBig(ex.jsonData.monsters[key].hpMult)
         }
      }
      
      for (var key in ex.jsonData.weapons)
      {
         for (var key2 in ex.jsonData.weapons[key])
         {
            if (ex.jsonData.weapons[key][key2].cost != undefined)
            {
               ex.jsonData.weapons[key][key2].cost = new uBig(ex.jsonData.weapons[key][key2].cost)
               ex.jsonData.weapons[key][key2].damage = new uBig(ex.jsonData.weapons[key][key2].damage)
            }
         }
      }      

      for (var key in ex.jsonData.goldLimit)
      {
         ex.jsonData.goldLimit[key] = new uBig(ex.jsonData.goldLimit[key])
      }

      console.log('jsonData',ex.jsonData)      
      //baseMosnterHp = new uBig(ex.jsonData.monsters
      // if (!ex.data.heroesData || ex.data.heroesData.length == 0)
         // uHeroes.PrecreateHeroes()
      // else
      // {
         // for (var i = 0; i < ex.data.heroesData.length; i++)
         // {
            // for (var j = 0; j < ex.data.heroesData[i].length; j++)
            // {
               // ex.data.heroesData[i][j] = uHeroes.HeroDataFromData(ex.data.heroesData[i])
            // }
         // }
      // }
      
      // if (!ex.data.monstersData || ex.data.monstersData.length == 0)
         // uMonsters.Precreate()
      // else
      // {
         // for (i = 0; i < ex.data.monstersData.length; i++)
         // {
            // ex.data.monstersData[i] = uMonsters.MonsterDataFromData(ex.data.monstersData[i])
         // }
      // }      
      
      // if (!ex.data.goldLimits || ex.data.goldLimits.length == 0)
      // {
         // ex.PrecreateGoldLimits()
      // }
      // else
      // {
         // for (var i = 0; i < ex.data.goldLimits.length; i++)
         // {
            // ex.data.goldLimits[i] = new uBig(ex.data.goldLimits[i])
         // }
      // }      
   // }
   // catch(err)
   // {
      // ex.data = 
      // {
         // goldLimits: [],
         // heroesData: [],
         // monstersData: [],
         // totalPreviousLevelsGold: []      
      // }
      // uHeroes.PrecreateHeroes()
      // uMonsters.Precreate()
      // ex.PrecreateGoldLimits()
      // ex.Save()
   //}
   
   //baseRefillSpeed = ex.RetrieveGoldLimit(1).div(60)
}

// ex.Save = function()
// {
   // uFs.writeFile('data/balanceData', JSON.stringify(ex.data), function(err)
   // {      
      // if (err)
         // throw err
   // })     
// }

// ex.PrecreateGoldLimits = function()
// {
   // for (var i = 0; i < ex.PRECREATE_GOLD_LIMITS; i++)
   // {
      // ex.data.goldLimits.push(ex.GetGoldLimit(i))
   // }
// }

// ex.GetMonsterGold = function(level)
// {
   // return coinIncreaseIncrement.pow(level).times(level).round()
// }

ex.GetWeaponDamage = function(weaponNumber,level)
{
   if (ex.jsonData.weapons["weapon"+weaponNumber][level] != undefined)
      return ex.jsonData.weapons["weapon"+weaponNumber][level].damage
   else
      return ex.jsonData.weapons["weapon"+weaponNumber].then.damage   
}

ex.GetWeaponCost = function(weaponNumber,level)
{
   if (ex.jsonData.weapons["weapon"+weaponNumber][level] != undefined)
      return ex.jsonData.weapons["weapon"+weaponNumber][level].cost
   else
      return ex.jsonData.weapons["weapon"+weaponNumber].then.cost   
}

ex.GetHeroDps = function(level)
{
   if (ex.jsonData.heroes[level] != undefined)
      return ex.jsonData.heroes[level].dps
   else
      return ex.jsonData.heroes.then.dps
}

ex.GetHeroCost = function(level)
{
   if (ex.jsonData.heroes[level] != undefined)
      return ex.jsonData.heroes[level].dps
   else
      return ex.jsonData.heroes.then.dps
}

ex.GetMonsterGold = function(level)
{
   if (ex.jsonData.monsters[level] != undefined)
      var mult = ex.jsonData.monsters[level].goldMult
   else
      mult = ex.jsonData.monsters.then.goldMult
   return ex.jsonData.monsters.baseGold.times(mult).round()
}

ex.GetMonsterHealth = function(level)
{
   if (ex.jsonData.monsters[level] != undefined)
      var mult = ex.jsonData.monsters[level].hpMult
   else
      mult = ex.jsonData.monsters.then.hpMult
   var result = ex.jsonData.monsters.baseHp.times(mult).round()
   //console.log('GetMonsterHealth returning',result)
   return result
}

// ex.GetMonsterHealth = function(level)
// {
   // var totalDps = uMonsters.RetrieveTotalPreviousLevelsGold(level).div(ex.DAMAGE_COST)
   // var difficulty = 2
   // if (level > difficulties.length)      
      // difficulty = difficulties[difficulties.length-1]+(3*(level-difficulties.length))
   // else
      // difficulty = difficulties[level-1]   
   // return totalDps.times(difficulty).round()
// }

ex.GetGoldLimit = function(level,totalGold)
{
   if (ex.jsonData.goldLimit[level] != undefined)
      return ex.jsonData.goldLimit[level]
   else
      return ex.jsonData.goldLimit.then.round()
}


// ex.GetGoldLimit = function(level,totalGold)
// {
   // var limit = ex.GetMonsterGold(level).times(uPreferences.MONSTERS_PER_LEVEL).plus(ex.GetMonsterHealth(level+1).div(2)).round()
   // if (level > 1)
   // {
      // limit = limit.plus(ex.data.goldLimits[level-2])
      // if (level > ex.data.goldLimits.length)
         // ex.data.goldLimits.push(limit)      
   // }
   // if (totalGold)
      // limit = limit.minus(totalGold)
   // if (limit.lt(uBigs.ZERO))
      // limit = uBigs.ZERO
   // return limit
// }

// ex.RetrieveGoldLimit = function(level,totalGold)
// {
   // var limit = ex.data.goldLimits[level]
   // if (!limit)
      // limit = ex.GetGoldLimit(level)
   // if (totalGold)
      // limit = limit.minus(totalGold)
   // if (limit.lt(uBigs.ZERO))
      // limit = uBigs.ZERO
   // return limit   
// }

// ex.GetGoldRefillSpeed = function(level)
// {
   // var thisLimit = ex.RetrieveGoldLimit(level)
   // var prevLimit = ex.RetrieveGoldLimit(level-1)
   // var diffLimit = thisLimit.minus(prevLimit)
   // return diffLimit.div(20*60)
// }

ex.GetArmorAndHp = function(level)
{
   var hp = ex.GetMonsterHealth(level)
   var result = { hp: hp, armor: uBigs.ZERO, hpArmor: hp }
   //console.log('GetArmorAndHp returning',result)
   return result
}

// ex.GetArmorAndHp = function(level)
// {
   // var hp = ex.GetMonsterHealth(level)
   // if (hp.lte(uBigs.BIG_150))
      // return { hp: hp, armor: uBigs.ZERO, hpArmor: hp }
   // else
   // {
      // var hpLeft = hp.minus(uBigs.BIG_150).div(uBigs.TWO)
      // var hpToArmor = hpLeft.div(ARMOR_DIVIDER).round(0,3)
      // if (hpToArmor.lt(uBigs.ONE))
         // hpToArmor = uBigs.ONE
      // var hpMinusArmor = hp.minus(hpLeft)
      // return { hp: hpMinusArmor, armor: hpToArmor, hpArmor: hp }
   // }
// }