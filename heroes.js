var uBig = require('big.js')
var uPreferences = require('./preferences')
var uMisc = require('./misc')
var uBalance = require('./balanceData')

var ex = module.exports

var heroNames = []

var baseZeroSeven = new uBig(1.07) //cost increase by 7%
var baseDamage = new uBig(5)
var baseCost = new uBig(50)
var baseDamageIncrease = new uBig(1.35)
var baseCostIncrease = new uBig(1)
//var baseCostIncrease = new uBig(1.1)
var baseHeroDamageCostIncrease = new uBig(1.05)

// function GetDamageCost(level)
// {
   // return uBalance.DAMAGE_COST.times(baseHeroDamageCostIncrease.pow(level-1))
// }

// function GetDamage(level)
// {
   // return baseDamage.times(baseDamageIncrease.pow(level - 1)).round(0,3)
// }

// function GetCost(level)
// {
   // return GetDamage(level+1).times(GetDamageCost(level+1)).round(0,3)
// }

// function GetDamage(level)
// {
   // return uBalance.GetHeroDps(level)
// }

// function GetCost(level)
// {
   // return uBalance.GetHeroCost(level)
// }

// function GetDamage(level)
// {
   // return baseDamage.times(baseDamageIncrease.pow(level - 1)).round(0,3)
// }

// function GetCost(level)
// {
   // return baseCost.times(baseCostIncrease.pow(level)).round(0,0)
// }

function HeroData(type,level,noNextLevel)
{
   this.type = type
   this.dps = {}
   switch(type) 
   {  
      case uPreferences.HERO_BARBARIAN:
         this.dps.type = uPreferences.DAMAGE_TYPE_SLASH
         break
      case uPreferences.HERO_RANGER:
         this.dps.type = uPreferences.DAMAGE_TYPE_RANGED
         break
      case uPreferences.HERO_MAGE:
         this.dps.type = uPreferences.DAMAGE_TYPE_SPELL
         break
      case uPreferences.HERO_ROGUE:
         this.dps.type = uPreferences.DAMAGE_TYPE_PIERCE
         break
      case uPreferences.HERO_ENGINEER:
         this.dps.type = uPreferences.DAMAGE_TYPE_SIEGE
         break
      case uPreferences.HERO_PALADIN:
         this.dps.type = uPreferences.DAMAGE_TYPE_HOLY
         break         
   }

   this.dps.value = uBalance.GetHeroDps(level)
   this.buyCost = uBalance.GetHeroCost(0)
   this.levelUpCost = uBalance.GetHeroCost(level)
   this.level = level
   if (noNextLevel == undefined)
      this.nextLevelHero = new HeroData(type,level+1,true)   
}

ex.HeroDataFromData = function(data)
{
   var noNextLevel = true
   if (data.nextLevelHero)
   {
      noNextLevel = false
      heroData.nextLevelHero = ex.HeroDataFromData(data.nextLevelHero)
   }
   var heroData = new HeroData(data.type,data.level,noNextLevel)
   heroData.dps.value = new uBig(data.dps.value)
   heroData.buyCost = new uBig(data.buyCost)
   heroData.levelUpCost = new uBig(data.levelUpCost)
   return heroData
}

// ex.PrecreateHeroes = function()
// {
   // uBalance.data.heroesData = []
   // for (var i = 0; i < uPreferences.NUMBER_OF_HEROES; i++)
   // {
      // uBalance.data.heroesData.push([])
      // for (var j = 0; j < uBalance.PRECREATE_HERO_LEVELS; j++)
      // {
         // uBalance.data.heroesData[i].push(new HeroData(i,j))
      // }
   // }   
// }


// ex.GetHeroDataByActualHero = function(actualHero)
// {
   // return ex.GetHeroData(actualHero.type, actualHero.level)
// }

// ex.GetHeroData = function(type,level)
// {
   // if (level >= uBalance.data.heroesData[0].length)
   // {
      // for (var i = 0; i < uPreferences.NUMBER_OF_HEROES; i++)
      // {      
         // var hdata = new HeroData(i,level)
         // uBalance.data.heroesData[i].push(hdata)
      // }
   // }
   // return uBalance.data.heroesData[type][level]
// }

ex.GetHeroData = function(type,level)
{
   return new HeroData(type,level)
}

ex.Affliction = function(type)
{
   this.type = type
   this.args = []
   this.timeAfflicted = uMisc.GetServerTime()
   switch(type)
   {
      case uPreferences.AFFLICTION_POISON:
         this.duration = 10
         this.args[0] = 50                               //% attack decrease         
         break
         
      case uPreferences.AFFLICTION_CHARM:
         this.duration = 10
         break
   }
}

ex.ActualHero = function(heroData, id)
{
   this.type = heroData.type
   this.level = heroData.level
   this.heroData = heroData
   //this.name = heroNames[uMisc.Random(0,heroNames.length)]
   this.id = id
   this.afflictions = []
}

ex.ActualHero.prototype.AddAffliction = function(afflictionType)
{
   this.afflictions.push(new ex.Affliction(afflictionType))
}

ex.ActualHero.prototype.IncreaseLevel = function(by)
{
   this.level += by
   this.heroData = ex.GetHeroData(this.type, this.level)
}

ex.ActualHero.prototype.RemoveAffliction = function(afflictionType)
{
   if (afflictionType == -1)
   {
      this.ClearAfflictions()
      return true
   }
   else
   {
      for (var i = 0; i < this.afflictions.length; i++)
      {
         if (this.afflictions[i].type === afflictionType)
         {
            this.afflictions.splice(i,1)
            return true
         }
      }
   }
   return false
}

ex.ActualHero.prototype.ClearAfflictions = function()
{
   this.afflictions = []
}

ex.ActualHero.prototype.HasAffliction = function(afflictionType)
{
   for (var i = 0; i < this.afflictions.length; i++)
   {
      if (this.afflictions[i].type === afflictionType)
      {
         return true
      }
   } 
   return false
}