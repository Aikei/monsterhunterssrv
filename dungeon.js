var uMisc = require('./misc')
var uPreferences = require('./preferences')
var uMonsters = require('./monsters')
var uSavedData = require('./savedData')

var ex = module.exports

var dungeonNames = []
var dice = 0

for (var i = 0; i < uPreferences.NUMBER_OF_DUNGEONS; i++)
{
   dungeonNames.push("Dungeon"+String(i+1))
}

ex.__dungeons = []

ex.EFFECT_MONSTERS_WITH_TRAITS = 0
ex.NUMBER_OF_EFFECTS = 1

ex.FAT_MONSTERS = 1
ex.FAST_MONSTERS = 2
ex.TOUGH_MONSTERS = 3
ex.SHADOW_MONSTERS = 4
ex.POISONOUS_MONSTERS = 5
ex.CHARMING_MONSTERS = 6

ex.LAST_EFFECT = 7

ex.allExhausted = false

ex.Dungeon = function(type,effects)
{
   this.type = type
   this.name = dungeonNames[type]
   this.possibleMonsters = []
   this.possibleEffects = []
   this.exhaustion = 0
   this.exhaustionTime = 0
   this.playersInDungeon = 0
   this.effects = []
   switch (type)
   {
      case uPreferences.DUNGEON_ORANGERY:
         this.possibleEffects = [ ex.EFFECT_MONSTERS_WITH_TRAITS ]
         this.possibleTraits = [ ex.POISONOUS_MONSTERS, ex.FAT_MONSTERS ]
         this.possibleMonsters = [ uPreferences.MONSTER_DUMMY, uPreferences.MONSTER_SLIME, uPreferences.MONSTER_SAPLING, uPreferences.MONSTER_PLANT ]
         break
      case uPreferences.DUNGEON_GRAVEYARD:
         this.possibleEffects = [ ex.EFFECT_MONSTERS_WITH_TRAITS ]
         this.possibleTraits = [ ex.TOUGH_MONSTERS, ex.SHADOW_MONSTERS ]
         this.possibleMonsters = [ uPreferences.MONSTER_BAT, uPreferences.MONSTER_SHADOW, uPreferences.MONSTER_DARK_DUMMY, uPreferences.MONSTER_LURKER ]
         break
      case uPreferences.DUNGEON_TWILIGHT_CRYPT:
         this.possibleEffects = [ ex.EFFECT_MONSTERS_WITH_TRAITS ]
         //this.possibleTraits = [ ex.CHARMING_MONSTERS ]
         this.possibleTraits = [ ex.CHARMING_MONSTERS, ex.SHADOW_MONSTERS ]
         this.possibleMonsters = [ uPreferences.MONSTER_SKELETON, uPreferences.MONSTER_SENTRY, uPreferences.MONSTER_GOBLIN, uPreferences.MONSTER_CRYPT_SLIME ]
         break
      case uPreferences.DUNGEON_GOBLIN_CITADEL:
         this.possibleEffects = [ ex.EFFECT_MONSTERS_WITH_TRAITS ]
         this.possibleTraits = [ ex.FAST_MONSTERS, ex.FAST_MONSTERS, ex.POISONOUS_MONSTERS ]
         this.possibleMonsters = [ uPreferences.MONSTER_DUMMY, uPreferences.MONSTER_SLIME, uPreferences.MONSTER_SAPLING, uPreferences.MONSTER_PLANT ]
         break         
   }
   if (effects)
   {
      this.effects = effects
   }
   else
   {
      this.ChangeEffects()
   }
}

ex.Dungeon.prototype.GetArmorTypesByShare = function()
{
   var result = []
   for (var i = 1; i < uPreferences.ARMOR_LAST; i++)
   {      
      result[i] = 0    
   }
   for (var j = 0; j < this.possibleMonsters.length; j++)
   {
      var defType = uMonsters.GetMonsterDefenseType(this.possibleMonsters[j])
      result[defType-1]++
   }
   for (var k = 1; k < uPreferences.ARMOR_LAST; k++)
   {      
      result[k] = result[k] / this.possibleMonsters.length
   }
   return result
}

ex.Dungeon.prototype.GetNewMonsterType = function()
{
   return this.possibleMonsters[uMisc.Random(0,this.possibleMonsters.length)]
}

ex.Dungeon.prototype.ChangeEffects = function()
{
   this.effects = []
   var effectType = this.possibleEffects[uMisc.Random(0,this.possibleEffects.length)]
   this.AddEffect(effectType)
}

ex.Dungeon.prototype.AddEffect = function(effectType,args)
{
   if (effectType === ex.EFFECT_MONSTERS_WITH_TRAITS)
   {
      var trait = this.possibleTraits[uMisc.Random(0,this.possibleTraits.length)]
      this.effects.push(new DungeonEffect(effectType,trait))
   }
   else
      this.effects.push(new DungeonEffect(effectType))   
}

ex.DungeonShell = function(type)
{
   this.type = type
   this.level = 1
   this.maxLevel = 1
   this.current = false
   this.monstersKilled = 0
}

ex.GetRandomDungeonType = function()
{
   return uMisc.Random(0,uPreferences.NUMBER_OF_DUNGEONS)
}

ex.Init = function()
{
   //console.log('saved data dungeons: ',uSavedData.data.dungeons)
   for (var i = 0; i < uPreferences.NUMBER_OF_DUNGEONS; i++)
   {
      if (uSavedData.data.dungeons)
         ex.__dungeons.push(new ex.Dungeon(i,uSavedData.data.dungeons[i].effects))
      else
         ex.__dungeons.push(new ex.Dungeon(i))
   }
   uSavedData.data.dungeons = ex.__dungeons
   //setInterval(CheckExhaustion, 10000)
}

ex.OnNewDay = function()
{
   for (var i = 0; i < ex.__dungeons.length; i++)
   {
      ex.__dungeons[i].ChangeEffects()
   }
   uSavedData.data.dungeons = ex.__dungeons
}

function CheckExhaustion()
{
   for (var i = 0; i < ex.__dungeons.length; i++)
   {
      if (ex.__dungeons[i].exhaustion < 100)
      {
         dice = uMisc.Random(0,2000)
         //dice = 0
         if (dice < ex.__dungeons[i].playersInDungeon)
         {
            ex.__dungeons[i].exhaustion += 1
            if (ex.__dungeons[i].exhaustion == 100)
            {
               ex.__dungeons[i].exhaustionTime = uMisc.GetServerTime()
            }            
         }
      }
      else if (uMisc.GetServerTime() - ex.__dungeons[i].exhaustionTime >= 648000)          //3 hours
      {
         dice = uMisc.ThrowDice()
         if (dice < 100)
         {
            ex.__dungeons[i].exhaustion = 0
            ex.__dungeons[i].ChangeEffects()
         }
      }
   }
}

function DungeonEffect(type,args)
{
   this.type = type
   this.args = []
   switch (this.type)
   {
      case ex.EFFECT_MONSTERS_WITH_TRAITS:
         if (args == undefined)
            this.args[0] = uMisc.Random(1, ex.LAST_EFFECT)   //trait type
         else
            this.args[0] = args
         dice = uMisc.ThrowDice()
         if (dice < uMisc.DICE_WIDTH/2)
            this.args[1] = uMisc.DICE_WIDTH*0.2     //trait chance
         else
            this.args[1] = uMisc.DICE_WIDTH*0.4     //trait chance
         break
   }
}
