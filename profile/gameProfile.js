var uSocialProfile = require('./socialProfile')
var uFs = require('fs')
var uDbg = require('../debug')
var uGame = require('../game')
var uGlobalStats = require('../globalStats')

var gameProfile = module.exports


gameProfile._profiles = {}

var lastWebId = 100
var saveLastIdInProcess = false

gameProfile.profile = function(socialProfile)
{
   this.socialProfile = socialProfile
   this.sqid = lastWebId++
   this.quitTime = 0
   gameProfile.SaveLastId()
   if (socialProfile.type === "web")
   {
      this.socialProfile.numId = this.sqid
      this.socialProfile.id = "web_"+this.sqid      
      this.socialProfile.name = "Web"
      this.socialProfile.lastName = "Player"+this.sqid
   }
   this.id = socialProfile.id
   this.online = false
   this.game = null
   this.sckstr = null
}

gameProfile.GetProfile = function(messageData,callback)
{
   var socialProfile = new uSocialProfile.profile(messageData)
   //console.log('received h: ',messageData.h)   
   uDbg.Log("_ii",socialProfile.id)
     
   var p = gameProfile._profiles[socialProfile.id]
   if (p === undefined)
   {
      uFs.readFile('data/profiles/'+socialProfile.id,function(err,data)
      {
         if (err)
         {
            p = gameProfile.CreateNewProfile(socialProfile)
            //console.log('saved h: ',p.socialProfile.h)            
         }
         else
         {
            p = gameProfile.LoadOldProfile(data)
            var receivedH = parseInt(socialProfile.h,16)
            var ourH = p.socialProfile.h
            //console.log('parsed received: ',receivedH)
           // console.log('our: ',ourH)
            if (socialProfile.type === "web" && socialProfile.h != p.socialProfile.h)
               p = gameProfile.CreateNewProfile(socialProfile)
         }
         callback(p)
      })      
   }
   else
      callback(p)   
}

gameProfile.CreateNewProfile = function(socialProfile)
{
   var p = new gameProfile.profile(socialProfile)
   gameProfile._profiles[socialProfile.id] = p
   p.game = new uGame.Game(socialProfile.id)
   gameProfile.SaveProfile(p)
   uGlobalStats.NewPlayerRegistered(p.id)
   return p
}

gameProfile.LoadOldProfile = function(data)
{
   var p = JSON.parse(data)
   gameProfile._profiles[p.socialProfile.id] = p 
   p.game = new uGame.Game(null,p.game)
   uGlobalStats.OnConnect(p.id)
   return p
}

// gameProfile.GetProfile = function(messageData)
// {
   ////uDbg.Log("_ii","In Get Profile")
   // var socialProfile = new uSocialProfile.profile(messageData)
   // var hash = gameProfile.GetSocialHash(socialProfile)
   // uDbg.Log("_ii",hash)
   
   ////return new gameProfile.profile(socialProfile)
   
   // var p = gameProfile._profiles[hash]
   // if (p === undefined)
   // {
      // uDbg.Log("p is undefined, creating new profile")
      // p = new gameProfile.profile(socialProfile)
      // gameProfile._profiles[hash] = p
      // gameProfile.SaveProfile(p) 
      // uFs.writeFileSync('data/lastid',lastWebId)      
   // }
   // return p
// }

// gameProfile.GetSocialHash = function(socialProfile)
// {
   // return socialProfile.type+"_"+socialProfile.id
// }

gameProfile.SaveProfile = function(profile)
{
   uFs.writeFile('data/profiles/'+String(profile.id), JSON.stringify(profile), function(err)
   {      
      if (err)
         throw err
   })
}

gameProfile.SaveAllProfiles = function()
{
   for (var key in gameProfile._profiles)
   {
      gameProfile.SaveProfile(gameProfile._profiles[key])
   }
}

gameProfile.LoadAllProfiles = function(callback)
{
   lastWebId = uFs.readFileSync('data/lastid')
   uFs.readdir('data/profiles/', function(err,files)
   {
      if (err)
         throw err
      if (files.length === 0)
      {
            callback()
            return
      }
      var n = 0
      for (var i = 0; i < files.length; i++)
      {
         uFs.readFile('data/profiles/'+files[i], function(err,data)
         {
            if (err)
               throw err
            var p = JSON.parse(data)
            gameProfile._profiles[p.socialProfile.id] = p
            n++
            if (n >= files.length)
            {
               callback()
            }
         })
      }
   })
}

gameProfile.Init = function()
{
   lastWebId = uFs.readFileSync('data/lastid')
}

gameProfile.SaveLastId = function()
{
   if (saveLastIdInProcess)
      return
   saveLastIdInProcess = true
   uFs.writeFile('data/lastid', lastWebId, function(err)
   {
      saveLastIdInProcess = false
      if (err)
         throw err
   })      
}

gameProfile.PrintAllProfiles = function()
{
   for (var key in gameProfile._profiles)
   {
      uDbg.Log("_ii","Loaded Profile: ",gameProfile._profiles[key].socialProfile.id)
   }
}