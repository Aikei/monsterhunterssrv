var uDbg = require('./debug')
var uFs = require('fs')
var uGameProfile = require('./profile/gameProfile.js')

var pathToProfiles = 'data/profiles/'
var pathToData = 'data/'

module.exports.ProcessArgs = function(args)
{
   for (var i = 2; i < args.length; i++)
   {
      if (args[i] === "clear_log")
      {
         uDbg.ClearLog()
      }
      else if (args[i] === "clear_saves")
      {
         ClearSaves()
      }
      else if (args[i] === "clear_leaderboard")
      {
         ClearLeaderboard()
      }
      else if (args[i] === "clear_stats")
      {
         ClearStats()
      }
      else if (args[i] === "clear_saved_data")
      {
         ClearSavedData()
      }
      else if (args[i] === "clear_balance_data")
      {
         ClearBalanceData()
      }
      else if (args[i] === "clear_player_logs")
      {
         uDbg.ClearPlayerLogs()
      }      
      else if (args[i] === "clear_all")
      {
         uDbg.ClearLog()
         uDbg.ClearPlayerLogs()
         ClearSaves()
         ClearLeaderboard()
         ClearStats()
         ClearSavedData()
         ClearBalanceData()
      }
   }   
}

function ClearStats()
{
   try
   {
      uFs.unlinkSync(pathToData+'globalStats')
   }
   catch (err)
   {
      uDbg.Log("_ii", "Error clearing stats: ", err)
   }
   uDbg.Log("_ii","Stats cleared")       
}

function ClearSavedData()
{
   try
   {
      uFs.unlinkSync(pathToData+'savedData')
   }
   catch (err)
   {
      uDbg.Log("_ii", "Error clearing stats: ", err)
   }
   uDbg.Log("_ii","Stats cleared")       
}

function ClearBalanceData()
{
   try
   {
      uFs.unlinkSync(pathToData+'balanceData')
   }
   catch (err)
   {
      uDbg.Log("_ii", "Error clearing stats: ", err)
   }
   uDbg.Log("_ii","Stats cleared")       
}

function ClearSaves()
{
   try
   {
      var files = uFs.readdirSync(pathToProfiles)
   }
   catch(err)
   {
      throw err
   }
   
   for (var j = 0; j < files.length; j++)
   {
      uFs.unlinkSync(pathToProfiles+files[j])
   }
   
   try
   {
      uFs.unlinkSync(pathToData+'lastid')
   }
   catch(err)
   {
      uDbg.Log(err)
   }
   
   uGameProfile.SaveLastId()
   uDbg.Log("_ii","Saves cleared")       
}

function ClearLeaderboard()
{
   try
   {
      uFs.unlinkSync(pathToData+'leaderboard')
   }
   catch (err)
   {
      uDbg.Log("_ii", "Error clearing leaderboard: ", err)
   }
   uDbg.Log("_ii","Leaderboard cleared")    
}