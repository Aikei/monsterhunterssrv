//misc.js
var uDbg = require('./debug')
var uBig = require('big.js')
var uPreferences = require('./preferences')
var uFs = require('fs')
var uMurmurHash = require('murmurhash-js')
var uRandom = require("random-js")()


var ex = module.exports

ex.DICE_WIDTH = 10000
ex.NAME_HASH_SEED = 154529485

ex.Hash = function(value) 
{
    return (typeof value) + ' ' + (value instanceof Object ?
        (value.__hash || (value.__hash = ++arguments.callee.current)) :
        value.toString());
}

ex.GetServerTime = function()
{
	var d = new Date();
	return d.getTime()/1000;
}

ex.IsMessageValid = function(msg)
{
   var str = String(msg.n)+String(msg.s)+JSON.stringify(msg.m_data)
   if (ex.GetCheckHash(str) == parseInt(msg.h,16))
      return true
   return false
}

ex.GetCheckHash = function(str)
{
   var a = 3
   if (str.length < 4)
      a = 0
   var b = 7
   if (str.length === 1)
      b = 0
   else if (str.length < 8)
      b = 2
   var hash = parseInt(uMurmurHash.murmur2(str, str.charCodeAt(a)+str.charCodeAt(b)))
   return hash
}

ex.GetNameHash = function(str)
{
   return parseInt(uMurmurHash.murmur2(str, ex.NAME_HASH_SEED+str.charCodeAt(0)))
}

// ex.GetCheckHash = function(str)
// {
   ////console.log('str2: ',str)
   // var charCodes = []
   // if (str.length > 1)
      // charCodes.push(str.charCodeAt(str.length - 2))      
   // for (var i = 0; i*2 < str.length; i++)
   // {
      // charCodes.push(str.charCodeAt(i*2))
   // }
   ////console.log('char codes:',charCodes)
   // var n = 0
   // for (var i = 0; i < charCodes.length; i++)
   // {
      // n += charCodes[i]
   // }
   // n *= charCodes[0]
   // console.log("hash: ",n)
   // return n
// }

ex.DeSerialize = function (str)
{
   var obj = null
   try
   {
      obj = JSON.parse(str);
   }
   catch(e)
   {
      uDbg.Log("_ii", "error parsing JSON string:", str)
      uDbg.Log("_ii", "JSON exception:", e)
      obj = null
   }
   finally
   {
      return obj;
   }
}

ex.Serialize = function (obj)
{
	var str = JSON.stringify(obj);
	return str;
}

ex.DeleteNullChars = function (str)
{
   var newstr = str.replace(/\0/g,'')
   return newstr
}

ex.ReplaceNullCharsWithNewlines = function (str)
{
   var newstr = str.replace(/\0/g,"\n")
   return newstr
}

ex.AddVisibleSpaces = function (str)
{
   var newstr = str.replace(/ /g,'_').replace(/\0/g,'%0%').replace(/\n/g,'%N%').replace(/\t/g,'%T%')
   return newstr
}

ex.Random = function (low, high) 
{
   return uRandom.integer(low,high-1)
   //return Math.floor(Math.random() * (high - low)) + low
}

ex.RandomNoFloor = function (low, high) 
{
    return Math.random() * (high - low) + low
}

ex.ThrowDice = function()
{
   return ex.Random(0, uPreferences.PROBABILITY_WIDTH)
}

ex.CopyArray = function(array)
{
   return array.slice()  
}

ex.Construct = function(constructor, args) 
{
    function F() 
    {
        return constructor.apply(this, args)
    }
    F.prototype = constructor.prototype
    return new F()
}

ex.SaveObject = function(obj,fileName)
{
   uFs.writeFile('data/'+fileName, JSON.stringify(obj), function(err)
   {      
      if (err)
         throw err
   })   
}

ex.LoadObjectSync = function(fileName)
{
   var file = uFs.readFileSync('data/'+fileName)
   if (file)
   {
      var obj = JSON.parse(file)
      return obj
   }
   else
      return null
}

ex.BIG_ZERO = new uBig(0)
ex.BIG_ONE = new uBig(1)
ex.BIG_ONE_HUNDRED = new uBig(100)

// ex.DeepCopy = function(obj)
// {
   // var n = {}
   // for (var key in obj)
   // {
      // n[key] = key
   // }
   // return n
// }

ex.Hash.current = 0;

