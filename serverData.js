var ex = module.exports

var uPreferences = require('./preferences')
var uMisc = require('./misc')
var uMonsters = require('./monsters')
var uWeapons = require('./weapons')
var uGame = require('./game')
var uHeroes = require('./heroes')
var uDungeon = require('./dungeon')

ex.Init = function()
{

   ex.data = {}

   //ex.heroes

   ex.data.heroes = []

   for (var i = 0; i < uPreferences.NUMBER_OF_HEROES; i++)
   {
      ex.data.heroes.push(uHeroes.GetHeroData(i,1))
   }

   //ex.afflictions
   
   ex.data.afflictions = []
   for (var i = 0; i < uPreferences.NUMBER_OF_AFFLICTIONS; i++)
   {
      ex.data.afflictions.push(new uHeroes.Affliction(i))
   }

   //ex.weapons

   ex.data.weapons = []       
   for (var i = 0; i < uPreferences.NUMBER_OF_WEAPONS; i++)
   {
      ex.data.weapons.push(uWeapons.CreateWeapon(i,1));
   }
   ex.data.dungeons = uDungeon.__dungeons
}


ex.GetServerData = function(myGame)
{
   if (myGame)
   {
      ex.data.game = myGame
   }
   else
   {
      ex.data.game = new uGame.Game()
   }
   ex.data.game.CreateNewMonster()
   return ex.data
}