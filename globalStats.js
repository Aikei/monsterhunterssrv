var uMisc = require('./misc')
var uMyDate = require('./myDate')

var uFs = require('fs')

var ex = module.exports

ex.globalStats = null

ex.Init = function()
{
   try
   {
      var f = uFs.readFileSync('data/globalStats', { encoding : 'utf8' } )
      ex.globalStats = JSON.parse(f)
   }
   catch(err)
   {
      ex.globalStats = new GlobalStats()
   }
}

ex.Save = function()
{
   uFs.writeFile('data/globalStats', JSON.stringify(ex.globalStats), function(err)
   {      
      if (err)
         throw err
   })   
}

ex.OnConnect = function(id)
{
   
   if (!ex.globalStats.profileStats[id])
   {
      ex.globalStats.profileStats[id] = new ProfileStats()
   }
   var stats = ex.globalStats.profileStats[id]
   stats.ses.push(new Session())
   if (!stats.wasOnline)
   {
      ex.globalStats.currentDay.players.push(id)
      stats.wasOnline = true
   }
   ex.globalStats.currentDay.ses.push(stats.ses[stats.ses.length-1])
}

ex.OnDisconnect = function(id)
{
   var obj = ex.globalStats.profileStats[id]
   var lastSession = obj.ses[obj.ses.length-1]
   lastSession.end = Math.round(uMisc.GetServerTime())
   lastSession.len = lastSession.end - lastSession.start
}

ex.NewPlayerRegistered = function(id)
{
   ex.globalStats.totalRegistered++
   ex.globalStats.profileStats[id] = new ProfileStats()
   ex.OnConnect(id)
}

ex.OnNewDay = function()
{
   for (var i = 0; i < ex.globalStats.currentDay.players.length; i++)
   {
      ex.globalStats.profileStats[ex.globalStats.currentDay.players[i]].wasOnline = false
   }
   ex.globalStats.currentDay = new Day()
   ex.globalStats.days.push(ex.globalStats.currentDay)  
}

function Day()
{
   this.date = new uMyDate.MyDate(uMisc.GetServerTime())
   this.ses = []
   this.players = []
}

function GlobalStats()
{
   this.totalRegistered = 0
   this.profileStats = {}
   this.currentDay = new Day()
   this.days = [ this.currentDay ]
}

function ProfileStats()
{
   this.regTime = Math.round(uMisc.GetServerTime())
   this.regDate = new uMyDate.MyDate(this.regTime)
   this.ses = []
   this.wasOnline = false
}

function Session()
{
   this.start = Math.round(uMisc.GetServerTime())
   this.end = -1
   this.len = -1
}

