var uBig = require('big.js')

var preferences = module.exports

//system
preferences.NODE_COMMAND = process.argv[0]
preferences.shuttingDown = false

//heroes

preferences.HERO_BARBARIAN = 0
preferences.HERO_RANGER = 1
preferences.HERO_MAGE = 2
preferences.HERO_ROGUE = 3
preferences.HERO_ENGINEER = 4
preferences.HERO_PALADIN = 5

preferences.NUMBER_OF_HEROES = 6

//damage type

preferences.DAMAGE_TYPE_NONE = 0
preferences.DAMAGE_TYPE_SLASH = 1
preferences.DAMAGE_TYPE_RANGED = 2
preferences.DAMAGE_TYPE_SPELL = 3
preferences.DAMAGE_TYPE_PIERCE = 4
preferences.DAMAGE_TYPE_SIEGE = 5
preferences.DAMAGE_TYPE_HOLY = 6

//armor type

preferences.ARMOR_NONE = 0
preferences.ARMOR_LIGHT = 1
preferences.ARMOR_NO_ARMOR = 2
preferences.ARMOR_MAGIC = 3
preferences.ARMOR_HEAVY = 4
preferences.ARMOR_FORTIFIED = 5
preferences.ARMOR_DARK = 6
preferences.ARMOR_LAST = 7

//dungeons

preferences.DUNGEON_ORANGERY = 0
preferences.DUNGEON_GRAVEYARD = 1
preferences.DUNGEON_TWILIGHT_CRYPT = 2
preferences.DUNGEON_GOBLIN_CITADEL = 3
preferences.NUMBER_OF_DUNGEONS = 3

//monsters

//orangery
preferences.MONSTER_DUMMY = 0
preferences.MONSTER_SLIME = 1
preferences.MONSTER_SAPLING = 2
preferences.MONSTER_PLANT = 3

//graveyard
preferences.MONSTER_BAT = 4
preferences.MONSTER_SHADOW = 5
preferences.MONSTER_DARK_DUMMY = 6
preferences.MONSTER_LURKER = 7

//crypt
preferences.MONSTER_SKELETON = 8
preferences.MONSTER_SENTRY = 9
preferences.MONSTER_GOBLIN = 10
preferences.MONSTER_CRYPT_SLIME = 11
      
preferences.NUMBER_OF_MONSTERS = 12

// preferences.MONSTER_BEHOLDER = 8

preferences.MONSTERS_PER_LEVEL = 10

//monsters' traits

preferences.NO_TRAIT = 0
preferences.TRAIT_FAT = 1
preferences.TRAIT_FAST = 2
preferences.TRAIT_TOUGH = 3
preferences.TRAIT_SHADOW = 4
preferences.TRAIT_POISONOUS = 5
preferences.TRAIT_CHARMER = 6

preferences.NUMBER_OF_TRAITS = 7

//monsters' rarity

preferences.RARITY_COMMON = 0
preferences.RARITY_RARE = 1
preferences.RARITY_ELITE = 2

//weapons

preferences.WEAPON_WOODEN_SWORD = 0
preferences.WEAPON_APPRENTICE_BLADE = 1
preferences.WEAPON_KNIGHTS_SWORD = 2
preferences.AMETHYST_BLADE = 3
preferences.ASSAULT_SWORD = 4
preferences.BONEMOURN = 5
preferences.INCINERATOR = 6

preferences.NUMBER_OF_WEAPONS = 7

//afflictions
preferences.AFFLICTION_POISON = 0
preferences.AFFLICTION_CHARM = 1
preferences.NUMBER_OF_AFFLICTIONS = 2

//probability
preferences.QUEST_PROBABILITY = 8
preferences.PROBABILITY_WIDTH = 10000
preferences.BASE_ARMOR_PROBABILITY = preferences.PROBABILITY_WIDTH / (preferences.ARMOR_LAST-1)


// preferences.RARE_HP_MULTIPLIER = new uBig(2)
// preferences.ELITE_HP_MULTIPLIER = new uBig(3)

// preferences.RARE_MONSTER_CHANCE = preferences.PROBABILITY_WIDTH/10
// preferences.ELITE_MONSTER_CHANCE = preferences.PROBABILITY_WIDTH/100

// preferences.RARE_MONSTER_CHANCE = preferences.PROBABILITY_WIDTH/3
// preferences.ELITE_MONSTER_CHANCE = preferences.PROBABILITY_WIDTH/10

// preferences.RARE_MONSTERS_SHARE = preferences.RARE_MONSTER_CHANCE / preferences.PROBABILITY_WIDTH
// preferences.ELITE_MONSTERS_SHARE = preferences.ELITE_MONSTER_CHANCE / preferences.PROBABILITY_WIDTH
// preferences.RARE_MONSTERS_SHARE_BIG = new uBig(preferences.RARE_MONSTERS_SHARE)
// preferences.ELITE_MONSTERS_SHARE_BIG = new uBig(preferences.ELITE_MONSTERS_SHARE)
// preferences.RARE_MONSTER_GLORY = new uBig(1)
// preferences.ELITE_MONSTER_GLORY = new uBig(3)
preferences.MAX_TIME_OFFLINE_BIG = new uBig(86400*2)

//skills
preferences.NUMBER_OF_SKILL_PACKS = 32