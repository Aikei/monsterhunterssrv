// var uJsSys = require('./js_system')
// uJsSys.InitSystem()

var net = require('net')
var http = require('http')

var uDbg = require('./debug')
var uMessage = require('./message')
var uPlayer = require('./player')
var uMisc = require('./misc')
var uGameProfile = require('./profile/gameProfile')
var uSocialProfile = require('./profile/socialProfile')
var uServerData = require('./serverData')
var uMonsters = require('./monsters')
var uWeapons = require('./weapons')
var uHeroes = require('./heroes')
var uDungeon = require('./dungeon')
var uSkills = require('./skills')
var uClArgs = require('./clArgs')
var uLeaderboard = require('./leaderboard')
var uCommands = require('./commandParser')
var uPreferences = require('./preferences')
var uGlobalStats = require('./globalStats')
var uSavedData = require('./savedData')
var uBalance = require('./balanceData')

var ex = module.exports

var port = 6600
//var port = 8500
var gameHttpPort = 10500
var host = "localhost"

var refreshIntervalId = 0
var updateIntervalId = 0
var update30IntervalId = 0

const kMessageTerminator = "\n"
const kNullCharacter = "\0"



/**
* Get the socket policy response for the given port number
*/
function GetPolicyResponse(port)
{
  var xml = '<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM'
          + ' "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n<cross-domain-policy>\n';
 
  xml += '<allow-access-from domain="*" to-ports="*"/>\n';
  xml += '<site-control permitted-cross-domain-policies="master-only"/>\n';
  xml += '</cross-domain-policy>\n\0';
 
  return xml;
}

ex.SendData = function (socket,data)
{
	socket.write(uMisc.Serialize(data)+kMessageTerminator+kNullCharacter);
}

function OnConnect(socket,socialProfileMessageData)
{
   var str = uPlayer.GetString(socket)
   if (!str)
      return
   socket.str = str
   uGameProfile.GetProfile(socialProfileMessageData, function(profile)
   {
      if (profile.online || uSavedData.IsCheater(socialProfileMessageData.id))
      {
         socket.destroy()
         return
      }
      profile.online = true
      if (uPlayer._players[str] === undefined)
      {
         //uDbg.Log('_ii','+++++++++++++++++++++++++++++++++++++++++++++')
         uDbg.Log('_ii','New player connected, ip: ', socket.remoteAddress,":",socket.remotePort,"id:",profile.id)  
         uDbg.Log('_ii',"authorization type: ", socialProfileMessageData.type, "id:", socialProfileMessageData.id,"name: ", socialProfileMessageData.name)      
         var player = new uPlayer.Player(socket,profile)
         uPlayer.AddPlayer(player)
         uPlayer.PrintPlayers()      
         var msg = new uMessage.Message(uMessage.Names.kConfirmConnect,undefined,uServerData.GetServerData(player.profile.game))
         msg.m_data.profile = profile
         msg.m_data.leaderboard = uLeaderboard.GetLeaderboard(profile.id)
         uMessage.SendMessage(socket,msg,true)
         //ex.SendData(socket,msg,true)
      }             
   })  
}

function OnClose(socket)
{   
  // uDbg.Log('_ii','Close received')
   //uDbg.Log('_ii','_____________________________________________')
   //var str = uPlayer.GetString(socket)
   uPlayer.RemovePlayerByStr(socket.str)
}

function Refresh()
{
   uPlayer.RemoveInactivePlayers()
   uLeaderboard.Update()
   
   //uPlayer.PrintPlayers()
   //setTimeout(Refresh,3000)
}

function Update()
{
   // for (var i = 0; i < uLogic._lookingForGame.length; i++)
   // {
      // uLogic.FindMatch(i)
   // }
   //uLogic.FindMatchForAll()
   setTimeout(Update,1000)
}

function ForgetPlayer(pl)
{
   uDbg.Log('Forgetting player: ',pl)
   uPlayer.RemovePlayerByStr(pl.GetString())
   //delete uPlayer._players[pl.GetString()]
   //uPlayer.PrintPlayers()
}

function OnTimeMessage(socket,obj)
{
   var serverTime = uMisc.GetServerTime()
   var timeToServer = serverTime - obj.m_data.m_clientTime;
   obj.m_data.m_serverTime = serverTime
   ex.SendData(socket,obj)
   var str = uPlayer.GetString(socket)
   var pl = uPlayer._players[str]
   if (pl != undefined)
   {
      pl.lastConnectTime = uMisc.GetServerTime()
   }
}

function OnGameMessage(socket,msg)
{
   var str = uPlayer.GetString(socket)
   var pl = uPlayer._players[str]
   if (pl === undefined)
   {
      uDbg.Log('Error! Player not connected!')
      return
   }
   pl.ProcessGameMessage(msg)
}

function OnFirstConnectMessage(socket,msg)
{
   var str = uPlayer.GetString(socket)
   var pl = uPlayer._players[str]
   if (pl === undefined)
   {
      uDbg.Log('Error! Player not connected!')
      return
   }
   pl.ProcessFirstConnectMessage(msg)
}

function OnFindMatchMessage(socket, msg)
{
   uDbg.Log("Find match message received")
   var str = uPlayer.GetString(socket)
   var pl = uPlayer._players[str]
   if (pl === undefined)
   {
      uDbg.Log('Error! Player not registed on server!')
      return
   }
   uDbg.Log("Finding match for player",pl.id)
}


// function OnServerInfo(socket,msg)
// {
   // msg.m_data = {}
   // var addr = server.address()
   // msg.m_data.ip = socket.localAddress
   // msg.m_data.port = socket.localPort
   // msg.m_data.numberOfPlayers = uPlayer.size
   // msg.m_data.lookingForGame = uLogic._lookingForGame.length
   // msg.m_data.numberOfGames = uLogic._games.size
   // ex.SendData(socket,msg)
// }

function OnServerMessage(socket,msg)
{
   uDbg.Log("_ii","Server message received: ", msg)
   if (msg.s === "newid")
   {
      
   }
   else if (msg.s === "restart")
   {
      process.exit(1)
   }
   // if (msg.s === "print_entropy")
   // {
      // var str = uPlayer.GetString(socket)
      // var pl = uPlayer._players[str]
      // if (pl != undefined && pl.inGame != undefined && uLogic._games[pl.inGame] != undefined)
      // {
         // uLogic._games[pl.inGame].PrintEntropy()
      // }
   // }
   // else if (msg.s === "print_dice")
   // {
      // var str = uPlayer.GetString(socket)
      // var pl = uPlayer._players[str]
      // if (pl != undefined && pl.inGame != undefined && uLogic._games[pl.inGame] != undefined)
      // {
         // uLogic._games[pl.inGame].PrintDice()
      // }
   // }
   // else if (msg.s === "givememana")
   // {
      // var str = uPlayer.GetString(socket)
      // var pl = uPlayer._players[str]
      // if (pl != undefined && pl.inGame != undefined && uLogic._games[pl.inGame] != undefined)
      // {
         // uLogic._games[pl.inGame].ChargeAll()
      // }
   // }     
}
 
var server = net.createServer(function(socket)
{
	socket.setEncoding('utf8')
	var socketData = ""
	
	socket.on('data', function(data)
	{
      //uDbg.Log('received: ',uMisc.AddVisibleSpaces(data))
		socketData += uMisc.DeleteNullChars(data)
		//uDbg.Log('socket data: ',uMisc.AddVisibleSpaces(socketData))
		if (socketData === '<policy-file-request/>')
		{
         uDbg.Log("policy requested, sending xml: ",xml);
			var xml = GetPolicyResponse(port)			
			socket.write(xml)
			socketData = ""
			return
		}
      
      //OnConnect(socket)
         
		var substrings = socketData.split(kMessageTerminator)
		var lastMsg = substrings.length-1

		if (substrings[lastMsg].length !== 0 && substrings[lastMsg] !== kNullCharacter)
		{
			// partial data read, store for later
			socketData = substrings[lastMsg]
		}
		else
		{
			//full read, clear buffer
			socketData = ""
		}
 
		// process all messages
      var wrongMessage = false
		for ( var i = 0; i<lastMsg; i++ )
		{
			var message = substrings[i];
         var obj = uMisc.DeSerialize(message)
         if (obj === null)
         {
            wrongMessage = true
            continue
         }         
         if (uMisc.IsMessageValid(obj))
         {
            // process message!
            uDbg.Log('message:',uMisc.AddVisibleSpaces(message))

            uDbg.Log("Got message: ",obj)
            if (obj.n === uMessage.Names.kTime)
            {
               OnTimeMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kGameMessage)
            {
               uDbg.Log("Game message received: ",obj)
               OnGameMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kServerMessage)
            {
               console.log("Server message received: ",obj)
               uDbg.Log("Server message received: ",obj)
               OnServerMessage(socket,obj)
            }
            // else if (obj.n === uMessage.Names.kServerInfo)
            // {
               // uDbg.Log("Server info requested")
               // OnServerInfo(socket,obj)
            // }
            // else if (obj.n === uMessage.Names.kListPlayers)
            // {
               // obj.m_data = []
               // for (var key in uPlayer._players)
               // {
                  // var curPl = new uPlayer.PlayerData(uPlayer._players[key])
                  // obj.m_data.push(curPl)
               // }
               // ex.SendData(socket,obj)
            // }
            else if (obj.n === uMessage.Names.kFirstConnect)
            {
               OnConnect(socket,obj.m_data)
               OnFirstConnectMessage(socket, obj)
            }
         }
		}
	});
 
	socket.on('close', function()
   {
      uDbg.Log('_ii','Close received')
      uDbg.Log('_ii','_____________________________________________')
      uPlayer.RemovePlayerByStr(socket.str)
   })
 
	socket.on('timeout', function(data)
	{
		uDbg.Log("timeout received");
	});
 
	socket.on('error', function(data)
	{
		uDbg.Log("error received");
	});
});

ex.CloseServer = function()
{
   console.log('closing server')
   clearInterval(refreshIntervalId)
   clearInterval(updateIntervalId)
   clearInterval(update30IntervalId)
   server.close()
   uPlayer.RemoveAllPlayers()
   uSavedData.Save()
   setTimeout(process.exit,90000)
}

var gameHttpServer = http.createServer(function(request, response)
{
	uDbg.Log("got http request: ",request.url);
	if (request.url === '/crossdomain.xml')
	{
		uDbg.Log("crossdomain.xml requested");
		var body = GetPolicyResponse(port);
		response.writeHead(200, {
		  'Content-Length': body.length,
		  'Content-Type': 'text/xml' });		
		response.end(body);
	}
});

process.on('exit', function(code) 
{
   console.log('server shut down')
   uDbg.LogSync("_ii","Exiting with code", code)
});

process.on('uncaughtException', function(err) 
{
   uDbg.LogSync("_ii","Uncaught exception: ", err)
   uDbg.LogSync("_ii","Exception stack: ", err.stack)
});


//every hour
function Update()
{
   uCommands.Update()
   var date = new Date()
   
   var hours = date.getUTCHours()
   
   var day = date.getUTCDate()
   var month = date.getUTCMonth()   
   var year = date.getUTCFullYear()
   
   var prevDay = uSavedData.prevDate.getUTCDate()
   var prevMonth = uSavedData.prevDate.getUTCMonth()
   var prevYear = uSavedData.prevDate.getUTCFullYear()  

   if ((day > prevDay || month > prevMonth || year > prevYear) && hours >= uSavedData.data.startHours)
   {
      ex.StartNewDay()
   }
}

function Update30()
{
   uGlobalStats.Save()
}

//every minute
// function Update()
// {
   // uCommands.Update()
   // var date = new Date()
   // var minutes = date.getMinutes()
   // var prevMinutes = previousDate.getMinutes()

   // if (minutes > prevMinutes || (prevMinutes == 59 && minutes == 0))
   // {
      // previousDate = date
      // StartNewDay()
   // }
// }

function SendNewDayMessage()
{
   var data = {}
   data.dungeons = uDungeon.__dungeons
   var msg = new uMessage.Message(uMessage.Names.kNewDay, "", data)
   for (var key in uPlayer._players)
   {
      uMessage.SendMessage(uPlayer._players[key].socket, msg, true)
   }
}

ex.StartNewDay = function()
{
   uDungeon.OnNewDay()
   uGlobalStats.OnNewDay()
   uLeaderboard.OnNewDay()
   uSavedData.OnNewDay()
   //SendNewDayMessage()
}

ex.Eval = function(command)
{
   eval(command)
}

uClArgs.ProcessArgs(process.argv)

console.log("Before init")
uDbg.Log("Before init")
console.log("1")
uSavedData.Init()
console.log("2")
uDbg.Init()
console.log("3")
uGlobalStats.Init()
console.log("4")
uLeaderboard.Init()
console.log("5")
uBalance.Init()
console.log("6")
uWeapons.Init()
console.log("7")
uDungeon.Init()
console.log("8")
uSkills.Init()
console.log("9")
uServerData.Init()
//ex.StartNewDay()
console.log("starting listening")
uDbg.Log("init done, starting listening")


server.listen(port)
gameHttpServer.listen(gameHttpPort)
refreshIntervalId = setInterval(Refresh,3000)
update30IntervalId = setInterval(Update30,30000)
updateIntervalId = setInterval(Update,60000)
// uDbg.Log("Before load profiles")
// uGameProfile.LoadAllProfiles(function ()
// {
   // uDbg.Log("After load profiles")
   ////uGameProfile.PrintAllProfiles()
   // server.listen(port)
   // gameHttpServer.listen(gameHttpPort)
   // Update()
   // Refresh()
// })

