//player.js
var uMisc = require('./misc')
var uDbg = require('./debug')
var uGameProfile = require('./profile/gameProfile')
var uGlobalStats = require('./globalStats')

var ex = module.exports

ex._players = {}

ex.size = 0
var timeToDisconnect = 60

ex.AddPlayer = function (player)
{
   var str = player.GetString()
   ex._players[str] = player
   ex.size++
}

ex.RemovePlayerByStr = function (str)
{
   var pl = ex._players[str]
   if (pl != undefined)
   {
      if (pl.socket)
         pl.socket.destroy()
      // if (pl.socket.remoteAddress != undefined)
         // pl.socket.end()
      uGlobalStats.OnDisconnect(pl.profile.id)
      pl.profile.game.OnPlayerLeave()
      pl.profile.online = false
      pl.profile.quitTime = uMisc.GetServerTime()
      delete uGameProfile._profiles[pl.profile.socialProfile.id]
      pl.SaveProfile()            
      delete ex._players[str]
      ex.size--
   }
}

ex.Player = function (socket, profile)
{
   if (arguments.length === 0)
      uDbg.Log("_ii","no arguments!")
   this.profile = profile
   this.profile.sckstr = socket.str
   this.socket = socket
   this.id = profile.id
   this.lastConnectTime = uMisc.GetServerTime()
}

ex.Player.prototype.ProcessGameMessage = function(message)
{
   this.profile.game.ProcessGameMessage(message,this.socket)
}

ex.Player.prototype.ProcessFirstConnectMessage = function(message)
{
   this.profile.game.ProcessFirstConnectMessage(message,this.socket)
}

ex.Player.prototype.IsConnected = function()
{
   return (this.socket.remoteAddress !== undefined)
}

ex.Player.prototype.Equals = function (other)
{
   if (socket.remoteAddress === other.socket.remoteAddress && id === other.id && socket.remotePort === other.socket.remotePort)
      return true
   return false      
}

ex.Player.prototype.GetString = function()
{
   var str = ex.GetString(this.socket)
   return str
}

ex.Player.prototype.SaveProfile = function()
{
   uGameProfile.SaveProfile(this.profile)
}

// ex.PlayerData = function(pl)
// {
   // this.id = pl.id
   // this.inGame = pl.inGame
   // this.ip = pl.socket.remoteAddress
   // this.port = pl.socket.remotePort
// }

ex.GetString = function(socket)
{
   if (!socket.remoteAddress)
      return null
   var str = socket.remoteAddress.toString()+'_'+socket.remotePort.toString()
   return str
}

ex.PrintPlayers = function()
{
   for (var key in ex._players)
   {
      uDbg.Log("key: ",key)
      var pl = ex._players[key]
      uDbg.Log('\nPlayer')
      uDbg.Log('id: ', pl.id)
      uDbg.Log('ip: ', pl.socket.remoteAddress)
      uDbg.Log('port: ', pl.socket.remotePort,'\n')
   }
}

ex.RemoveAllPlayers = function()
{
   for (var key in ex._players)
   {
      ex.RemovePlayerByStr(key)
   }
}

ex.RemoveInactivePlayers = function()
{
   //uDbg.Log('_ii','In remove inactive players')
   for (var key in ex._players)
   {
      //uDbg.Log('_ii','Checking key',key)
      var pl = ex._players[key]
      var timeDiff = uMisc.GetServerTime() - pl.lastConnectTime
      //uDbg.Log("_ii","time diff for player",pl.id,"=",timeDiff)
      // if (timeDiff > 8)
      // {
         // uLogic._games[pl.id]
      // }
      if (timeDiff > timeToDisconnect)
      {
         //uDbg.Log('_ii','Removing player',pl.id)      
         ex.RemovePlayerByStr(key)
         //ex.RemovePlayer(ex._players[key])
      }
   }
}