var uMisc = require('./misc')
var uDungeon = require('./dungeon')
var uPreferences = require('./preferences')
var uMonsters = require('./monsters')
var uBigs = require('./bigInts')
var uBig = require('big.js')
var uBalance = require('./balanceData')

var ex = module.exports
var dice = 0

ex.QUEST_TYPE_KILL = 0
ex.QUEST_TYPE_KILL_BOSS = 1
ex.QUEST_TYPE_REACH_LEVEL = 2

ex.QUEST_TYPES_NUMBER = 2


ex.QUEST_TARGET_ANY = 0
ex.QUEST_TARGET_SPECIFIC = 1

ex.QUEST_DUNGEON_ANY = 0
ex.QUEST_DUNGEON_SPECIFIC = 1

ex.QUEST_DIFFICULTY_EASY = 0
ex.QUEST_DIFFICULTY_MODERATE = 1
ex.QUEST_DIFFICULTY_HARD = 2

var easyQuests = [ ex.QUEST_TYPE_KILL ]
var moderateQuests = [ ex.QUEST_TYPE_KILL, ex.QUEST_TYPE_KILL_BOSS ]
var hardQuests = [ ex.QUEST_TYPE_KILL, ex.QUEST_TYPE_KILL_BOSS ]

ex.Quest = function(data)
{
   this.ac = false        //whether the quest is accepted
   if (data == undefined)
   {
      this.t = -1    //quest type
      this.ar = {}   //quest arguments
   }
   else
   {
      for (var key in data)
      {
         this[key] = data[key]
      }
   }
}

ex.GetRandomQuest = function(game)
{
   var df = uMisc.Random(0,100)
   var type = -1
   if (df < 5)
   {
      df = ex.QUEST_DIFFICULTY_HARD
      type = hardQuests[uMisc.Random(0,hardQuests.length)]
   }
   else if (df < 30)
   {
      df = ex.QUEST_DIFFICULTY_MODERATE
      type = moderateQuests[uMisc.Random(0,moderateQuests.length)]
   }
   else
   {
      df = ex.QUEST_DIFFICULTY_EASY
      type = easyQuests[uMisc.Random(0,easyQuests.length)]
   }

   var type = uMisc.Random(0,ex.QUEST_TYPES_NUMBER)

   return ex.CreateNewQuest(type,game)
}

function GetGoldReward(level,difficulty)
{
   var gold = uBalance.GetMonsterGold(level).times(uMisc.Random(uBalance.jsonData.quest.goldMultMin,uBalance.jsonData.quest.goldMultMax))
   
   if (difficulty === ex.QUEST_DIFFICULTY_EASY)
      gold = gold.times(uBalance.jsonData.quest.easyMult).round()
   else if (difficulty === ex.QUEST_DIFFICULTY_MODERATE)
      gold = gold.times(uBalance.jsonData.quest.moderateMult).round()
   else if (difficulty === ex.QUEST_DIFFICULTY_HARD)
      gold = gold.times(uBalance.jsonData.quest.hardMult).round()
   return gold
}

function GetGloryReward(game,level,difficulty)
{
   var hpShare = game.GetHpShare(level)
   var glory = new uBig(uMisc.Random(uBalance.jsonData.quest.gloryMultMin,uBalance.jsonData.quest.gloryMultMax))
   if (difficulty === ex.QUEST_DIFFICULTY_EASY)
      glory = glory.times(uBalance.jsonData.quest.easyMult).round()
   if (difficulty === ex.QUEST_DIFFICULTY_MODERATE)
      glory = glory.times(uBalance.jsonData.quest.moderateMult).round()
   else if (difficulty === ex.QUEST_DIFFICULTY_HARD)
      glory = glory.times(uBalance.jsonData.quest.hardMult).round()
   glory = glory.times(hpShare).round()
   return glory
}

ex.CreateNewQuest = function(t,game,difficulty)
{
   var q = new ex.Quest()
   q.t = t
   q.p = 0 //progress
   q.dt = {}
   var dungeon = null
   var additionalGoldMultiplier = uBigs.ONE
   var additionalGloryMultiplier = uBigs.ONE
   
   switch (q.t)
   {
      case ex.QUEST_TYPE_KILL:
         if (difficulty != ex.QUEST_DIFFICULTY_EASY)
            dice = 99
         else
            dice = uMisc.Random(0,100)
         q.ar.r = uPreferences.RARITY_COMMON
         if (dice < 20)
         {
            q.ar.mt = -1
            q.ar.num = uMisc.Random(2,6)*15
            q.ar.dun = game.GetLowestLevelDungeonType()           
         }
         else
         {
            q.ar.dun = uDungeon.GetRandomDungeonType()
            dungeon = uDungeon.__dungeons[q.ar.dun]
            q.ar.mt = dungeon.possibleMonsters[uMisc.Random(0,dungeon.possibleMonsters.length)]
            //q.ar.num = 1
            q.ar.num = uMisc.Random(2,5)*5 
            if (difficulty === ex.QUEST_DIFFICULTY_MODERATE)
               q.ar.num *= 2
            else if (difficulty === ex.QUEST_DIFFICULTY_HARD)
               q.ar.num *= 4
         }
         q.ar.l = game.dungeonShellsList[q.ar.dun].maxLevel
         q.pg = q.ar.num //progress goal
         break
         
      case ex.QUEST_TYPE_REACH_LEVEL:
         q.ar.dun = uDungeon.GetRandomDungeonType()
         q.ar.l = game.dungeonShellsList[q.ar.dun].level
         q.p = q.ar.l
         if (q.ar.l == game.maxDungeonLevel)
         {
            q.ar.l += uMisc.Random(5,15)
         }
         else
         {
            q.ar.l += uMisc.Random(10, 35)
         }
         q.pg = q.ar.l //progress goal
         break
         
     case ex.QUEST_TYPE_KILL_BOSS:
         q.ar.dun = uDungeon.GetRandomDungeonType()
         dungeon = uDungeon.__dungeons[q.ar.dun]
         q.ar.mt = dungeon.possibleMonsters[uMisc.Random(0,dungeon.possibleMonsters.length)]
         q.pg = -1
         q.ar.prob = 3
         if (difficulty == ex.QUEST_DIFFICULTY_MODERATE)
         {
            q.ar.prob = 2
         }
         else if (difficulty == ex.QUEST_DIFFICULTY_HARD)
         {
            q.ar.prob = 1
            additionalGoldMultiplier = uBigs.TWO
            additionalGloryMultiplier = uBigs.TWO
         }
         q.ar.l = game.dungeonShellsList[q.ar.dun].maxLevel
         break
   }
   q.rw = {}
   
   q.rw.gl = GetGloryReward(game,game.dungeonShellsList[q.ar.dun].maxLevel,difficulty).times(additionalGloryMultiplier).plus(uBigs.ONE)
   q.rw.go = GetGoldReward(game.dungeonShellsList[q.ar.dun].maxLevel,difficulty).times(additionalGoldMultiplier).plus(uBigs.ONE)
   
   q.rw.gl = q.rw.gl.plus(q.rw.gl.times(game.talentEffects.questRewardsIncrease/100))
   q.rw.go = q.rw.go.plus(q.rw.go.times(game.talentEffects.questRewardsIncrease/100))
   
   return q
}

ex.Quest.prototype.OnNewMonster = function(game)
{
   // if (this.t == ex.QUEST_TYPE_KILL_BOSS)
   // {
      // if (game.dungeonShellsList[game.currentDungeon].type === this.ar.dun && game.currentMonster.rarity === uPreferences.RARITY_ELITE && uMisc.Random(0,100) < this.ar.prob)
         // game.currentMonster.sp = "t"
   // }
}

ex.Quest.prototype.MeetsReqs = function(game)
{
   if (this.t == ex.QUEST_TYPE_KILL || this.t == ex.QUEST_TYPE_KILL_BOSS)
   {
      if (game.GetCurrentDungeonShell().level >= this.ar.l)
         return true
      else
         return false
   }
   return true
}

ex.Quest.prototype.OnMonsterKilled = function(monster,game)
{
   if (!this.ac)
      return
   if (this.t == ex.QUEST_TYPE_KILL)
   {
      if (this.MeetsReqs(game) && (this.ar.mt == -1 || (this.ar.mt === monster.type && this.ar.r === monster.rarity)))
      {
         this.p++
         if (this.p >= this.pg)
            this.p = -1
         return true
      }
   }
   else if (this.t == ex.QUEST_TYPE_KILL_BOSS)
   {
      if (monster.sp === "t")
      {
         this.p = -1
         return true
      }
   }
   return false
}
