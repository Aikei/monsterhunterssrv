//sorting leaderboard in a child process. receives leaderboard through process.stdin and sends it back in json to main process through process.stdout

var uBig = require('big.js')

var allData = ""
var leaderboard = null
var entriesMap = null
var perDaySort = false

if (process.argv[2] == "perDay")
{
   perDaySort = true
}

function CompareEntriesByTotalGlory(entryA,entryB)
{
   if (entryA.g.gt(entryB.g))
      return -1
   else
      return 1
}

function CompareEntriesByGloryPerDay(entryA,entryB)
{
   if (entryA.dg.gt(entryB.dg))
      return -1
   else
      return 1
}

process.stdin.setEncoding('utf8')
process.stdout.setEncoding('utf8')

process.stdin.on('data', function(data)
{
   allData += data
   if (data.charAt(data.length-1) == '\0')
   {
      allData = allData.slice(0,allData.length-1)
      AllDataRead()
   }
})

function AllDataRead()
{
   entriesMap = JSON.parse(allData)
   allData = ""
   leaderboard = []
   for (var key in entriesMap)
   {
      if (!perDaySort)
         entriesMap[key].g = new uBig(entriesMap[key].g)
      else
         entriesMap[key].dg = new uBig(entriesMap[key].dg)
      leaderboard.push(entriesMap[key])      
   }
   if (!perDaySort)
      leaderboard.sort(CompareEntriesByTotalGlory)
   else
      leaderboard.sort(CompareEntriesByGloryPerDay)
   process.stdout.write(JSON.stringify(leaderboard)+'\0')   
}
