const PRECREATE_LEVELS = 200

var uBig = require('big.js')
var uBigs = require('./bigInts')
var uPreferences = require('./preferences')
var uMisc = require('./misc')
var uDungeon = require('./dungeon')
var uQuest = require('./quest')
var uBalance = require('./balanceData')
var uBalance = require('./balanceData')
var uDbg = require('./debug')

var ex = module.exports

//var monstersData = []

const BIG_ZERO = new uBig(0)
const BIG_ONE = new uBig(1)
const BIG_TWO = new uBig(2)
const BIG_THREE = new uBig(3)
const BIG_TEN = new uBig(10)


const BASE_TRAIT_PERCENT = 0.2

const ADD_PERCENT = 1.0+BASE_TRAIT_PERCENT
const SUB_PERCENT = 1.0-BASE_TRAIT_PERCENT

const ADD_PERCENT_BIG = new uBig(ADD_PERCENT)
const SUB_PERCENT_BIG = new uBig(SUB_PERCENT)

const MIN_COINS = 3
const MAX_COINS = 6


const BIG_50 = new uBig(50)
const BIG_150 = new uBig(150)
const BIG_250 = new uBig(250)
const BIG_500 = new uBig(500)
const BIG_1000 = new uBig(1000)


var baseLife = new uBig(10)
var onePointSix = new uBig(1.6)
var onePointZeroSeven = new uBig(1.07)
var tenPercentMult = new uBig(1.1)
var levelMinusOneTimes10 = null
var dice = 0

var MonsterData = function(level)
{
   var hpAndArmor = uBalance.GetArmorAndHp(level)
   this.level = level
   this.hp = hpAndArmor.hp
   //console.log('MonsterData hp:',this.hp)
   this.armor = hpAndArmor.armor
   this.hpArmor = hpAndArmor.hpArmor
   this.gold = uBalance.GetMonsterGold(level)
}

ex.MonsterDataFromData = function(data)
{
   data.hp = new uBig(data.hp)
   data.armor = new uBig(data.armor)
   data.hpAndArmor = new uBig(data.hpAndArmor)
   data.gold = new uBig(data.gold)
   return data
}

MonsterData.prototype.Copy = function()
{
   var newData = {}
   newData.hp = this.hp
   newData.armor = this.armor
   //console.log('mosnters data copy, this.gold is bigint:',(this.gold.times != undefined))
   newData.gold = this.gold
   //console.log('mosnters data copy, newData.gold is bigint:',(newData.gold.times != undefined))
   newData.Copy = this.Copy
   return newData
}

ex.GetMonsterDefenseType = function(type)
{
   switch (type)
   {
      //orangery
      
      case uPreferences.MONSTER_DUMMY:
         return uPreferences.ARMOR_LIGHT;
         break
         
      case uPreferences.MONSTER_SLIME:
         return uPreferences.ARMOR_NO_ARMOR;
         break
         
      case uPreferences.MONSTER_SAPLING:
         return uPreferences.ARMOR_NO_ARMOR;
         break
         
      case uPreferences.MONSTER_PLANT:
         return uPreferences.ARMOR_NO_ARMOR;
         break
		
      //graveyard
      
      case uPreferences.MONSTER_BAT:
         return uPreferences.ARMOR_DARK;
         break
         
      case uPreferences.MONSTER_SHADOW:
         return uPreferences.ARMOR_DARK;
         break
         
      case uPreferences.MONSTER_DARK_DUMMY:
         return uPreferences.ARMOR_DARK;
         break
         
      case uPreferences.MONSTER_LURKER:
         return uPreferences.ARMOR_FORTIFIED;
         break		 
      
      //crypt
      
       case uPreferences.MONSTER_SKELETON:
         return uPreferences.ARMOR_HEAVY;
         break
         
      case uPreferences.MONSTER_SENTRY:
         return uPreferences.ARMOR_HEAVY;
         break
         
      case uPreferences.MONSTER_GOBLIN:
         return uPreferences.ARMOR_HEAVY;
         break
         
      case uPreferences.MONSTER_CRYPT_SLIME:
         return uPreferences.ARMOR_MAGIC;
         break	     
         
      default:
         return uPreferences.ARMOR_NO_ARMOR;
         break          
   }   
}

var ActualMonster = function(type,level,rarity,game)
{
   this.type = type
   this.rarity = rarity
   this.glory = uBigs.ZERO
   this.level = level
   this.sp = "0"
   
   this.creationTime = uMisc.GetServerTime()
   this.timerDuration = 30
  
   this.originalData = new MonsterData(level)
   this.monsterData = this.originalData.Copy()
   
   // if (level >= uBalance.data.monstersData.length)
   // {
      // uBalance.data.monstersData[level] = new MonsterData(level)
   // }
   // this.originalData = uBalance.data.monstersData[level]
   // this.monsterData = uBalance.data.monstersData[level].Copy()

   if (game.talentEffects.overallGoldIncrease > 0)
   {
      this.monsterData.gold = gold.plus(this.monsterData.gold.div(uBigs.ONE_HUNDRED).times(new uBig(game.talentEffects.overallGoldIncrease))).round(0,2)
   }   
   
   if (rarity === uPreferences.RARITY_RARE)
   {
      //this.glory = uPreferences.RARE_MONSTER_GLORY
      this.glory = new uBig(uBalance.jsonData.rare.rareMonsterGlory)
      this.monsterData.hp = this.monsterData.hp.times(uBalance.jsonData.rare.rareHpMultiplier) 
      //this.monsterData.hp = this.monsterData.hp.times(uPreferences.RARE_HP_MULTIPLIER)      
      //this.monsterData.gold = this.monsterData.gold.times(uBigs.TWO)
      this.MultiplyNumberOfCoins(uBalance.jsonData.rare.rareCoinsMultiplier)
   }
   else if (rarity === uPreferences.RARITY_ELITE)
   {      
      //this.glory = uPreferences.ELITE_MONSTER_GLORY
      this.glory = new uBig(uBalance.jsonData.rare.eliteMonsterGlory)
      this.monsterData.hp = this.monsterData.hp.times(uBalance.jsonData.rare.eliteHpMultiplier) 
      //this.monsterData.hp = this.monsterData.hp.times(uPreferences.ELITE_HP_MULTIPLIER)            
      //this.monsterData.gold = this.monsterData.gold.times(uBigs.THREE)
      this.MultiplyNumberOfCoins(uBalance.jsonData.rare.eliteCoinsMultiplier)
   }   
   
   if (game.goldLimit.gt(uBigs.ZERO) || game.GetCurrentDungeonShell().level == game.maxDungeonLevel)
   {
      this.coinsData = this.GetCoinsData(this.monsterData.gold)
   }
   else
   {
      this.coinsData = null
      //this.glory = uBigs.ZERO
   }
   
   if (this.coinsData && game.talentEffects.coinsIncrease > 0)
   {
      for (var i = 0; i < game.talentEffects.coinsIncrease; i++)
      {
         var d = uMisc.Random(0,100)
         if (d < game.talentEffects.addCoinChance)
         {
            this.coinsData.number++
         }
      }
   }   
     
   this.traits = []     
   this.armorType = ex.GetMonsterDefenseType(this.type)
}

ActualMonster.prototype.MultiplyNumberOfCoins = function(by)
{
   if (this.coinsData)
      this.coinsData.number *= by
}

ActualMonster.prototype.GetCoinsData = function(gold)
{
   var n = uMisc.Random(MIN_COINS,MAX_COINS)
   var w = gold.div(n).round(0,0)
   while(w.lt(1))
   {
      n--
      w = gold.div(n).round(0,0)
   }
   return { number: n, worth: w }  
}

ActualMonster.prototype.AddTrait = function(trait)
{
   this.traits.push(trait)
   switch (trait.id)
   {
      case uPreferences.TRAIT_FAT:
         this.monsterData.hp = this.monsterData.hp.times(new uBig(1+trait.args[0]/100)).round(0,3)
         break
         
      case uPreferences.TRAIT_FAST:
         this.timerDuration = Math.ceil(this.timerDuration*(1-trait.args[0]/100))
         break
         
      case uPreferences.TRAIT_TOUGH:
         this.monsterData.armor = this.monsterData.armor.times(new uBig(1+trait.args[0]/100)).round(0,3)
         if (this.monsterData.armor.lt(BIG_ONE))
            this.monsterData.armor = BIG_ONE
         break      
   }
}

var Trait = function(traitId)
{
   this.id = traitId
   this.args = []
   switch (traitId)
   {
      case uPreferences.TRAIT_FAT:
         this.args.push(35) //% hp increase
         break
      case uPreferences.TRAIT_FAST:
         this.args.push(35) //% timer decrease
         break
      case uPreferences.TRAIT_TOUGH:
         this.args.push(50) //% armor increase
         break
      case uPreferences.TRAIT_SHADOW:
         this.args.push(50) //% non-effective attacks decrease
         break
      case uPreferences.TRAIT_POISONOUS:
         this.args[0] = 15  //% chance to contract affliction per second
         this.args[1] = uPreferences.AFFLICTION_POISON //affliction id
         break
      case uPreferences.TRAIT_CHARMER:
         this.args[0] = 20  //% chance to contract affliction per second
         this.args[1] = uPreferences.AFFLICTION_CHARM //affliction id        
         break
   }
}

// ex.Precreate = function()
// {  
   // for (var i = 1; i <= uBalance.PRECREATE_MONSTER_LEVELS; i++)
   // {
      // ex.CreateNewMonsterData(i);
   // }   
// }

// ex.CreateNewMonsterData = function(level)
// {
   // var gold = uBalance.GetMonsterGold(level)
   // if (level > 1)
      // uBalance.data.totalPreviousLevelsGold[level] = uBalance.data.totalPreviousLevelsGold[level-1].plus(gold.times(uPreferences.MONSTERS_PER_LEVEL))
   // else
      // uBalance.data.totalPreviousLevelsGold[level] = uBalance.DAMAGE_COST.times(6)
   // uBalance.data.monstersData[level] = new MonsterData(level)
// }

// ex.RetrieveTotalPreviousLevelsGold = function(level)
// {
   // if (!uBalance.data.totalPreviousLevelsGold[level])
   // {
      // var gold = uBalance.GetMonsterGold(level)
      // uBalance.data.totalPreviousLevelsGold[level] = uBalance.data.totalPreviousLevelsGold[level-1].plus(gold.times(uPreferences.MONSTERS_PER_LEVEL))
   // }
   // return uBalance.data.totalPreviousLevelsGold[level]
// }

// ex.RetrieveMonsterGold = function(level)
// {
   // if (uBalance.data.monstersData[level] == undefined)
      // ex.CreateNewMonsterData(level)
   // return uBalance.data.monstersData[level].gold
// }

// ex.RetrieveMonsterHealth = function(level)
// {
   // uDbg.Log("retrieving mosnter health, level:",level)
   // if (uBalance.data.monstersData[level] == undefined)
   // {
      // uDbg.Log("monster data level",level,"not found, creating new monster data")
      // ex.CreateNewMonsterData(level)
   // }
   // uDbg.Log("returning health of monsterData =",uBalance.data.monstersData[level],"healthAndArmor =",uBalance.data.monstersData[level].hpArmor)
   // return uBalance.data.monstersData[level].hpArmor
// }

ex.GetHpShare = function(level,maxLevel)
{
   if (level < maxLevel)
   {
      //return uBalance.data.monstersData[level].hp.div(uBalance.data.monstersData[maxLevel].hp)
      return uBalance.GetMonsterHealth(level).div(uBalance.GetMonsterHealth(maxLevel))
   }
   return uBigs.ONE
}

ex.GetRareChances = function(level,maxLevel,talentEffects)
{
   var eliteChance = (uPreferences.PROBABILITY_WIDTH*uBalance.rareMonsterChance)+(talentEffects.eliteChanceIncrease*(uPreferences.PROBABILITY_WIDTH/100))
   var rareChance = (uPreferences.PROBABILITY_WIDTH*uBalance.eliteMonsterChance)+(talentEffects.rareChanceIncrease*(uPreferences.PROBABILITY_WIDTH/100))
   if (level < maxLevel)
   {
      var mHpNow = ex.RetrieveMonsterHealth(level)
      var hpMax = ex.RetrieveMonsterHealth(maxLevel)
      var relBig = mHpNow.div(hpMax)
      var rel = Number(relBig.toFixed(3))
      eliteChance *= rel
      rareChance *= rel
   }
   return { rare: rareChance, elite: eliteChance }
}

ex.GetMonster = function(game,noRare)
{
   var level = game.dungeonShellsList[game.currentDungeon].level
   var dungeon = uDungeon.__dungeons[game.currentDungeon]
   
   var numberOfTraits = 0
   var rarity = uPreferences.RARITY_COMMON
   var type = dungeon.GetNewMonsterType()
   var questBoss = false

   if (!noRare && game.q && game.q.ac && game.q.MeetsReqs(game) && game.q.t === uQuest.QUEST_TYPE_KILL_BOSS && game.q.ar.dun === game.dungeonShellsList[game.currentDungeon].type)
   {
      if (game.rareLoaded && uMisc.Random(0,100) < game.q.ar.prob)
      {
         rarity = uPreferences.RARITY_ELITE
         numberOfTraits = 2
         questBoss = true
      }
   }
   else if (!noRare)
   {
      var chances = ex.GetRareChances(level,game.maxDungeonLevel,game.talentEffects)
      if (game.rareLoaded)
      {
         var dice = uMisc.ThrowDice()
         if (dice < chances.elite)
         {
            rarity = uPreferences.RARITY_ELITE
            numberOfTraits = 2
         }
         else if (dice < chances.rare)
         {
            rarity = uPreferences.RARITY_RARE
            numberOfTraits = 1
         }
      }
   }
   
   // if (uBalance.data.monstersData[level] == undefined)
   // {
      // ex.CreateNewMonsterData(level)
   // }   
   
   var monster = new ActualMonster(type,level,rarity,game)    
   
   if (dungeon.effects.length > 0)
   {
      for (var i = 0; i < dungeon.effects.length; i++)
      {
         if (dungeon.effects[i].type === uDungeon.EFFECT_MONSTERS_WITH_TRAITS)
         {
            dice = uMisc.ThrowDice()
            if (dice < dungeon.effects[i].args[1])
               monster.AddTrait(new Trait(dungeon.effects[i].args[0]))
         }
      }
   }     
   
   f:
   for (var i = 0; i < numberOfTraits; i++)
   {
      var same = true
      var n = 0
      if (monster.traits.length == 0 && dungeon.effects[i].type === uDungeon.EFFECT_MONSTERS_WITH_TRAITS)
      {
         monster.AddTrait(new Trait(dungeon.effects[i].args[0]))
      }
      else
      {
         if (monster.traits.length < dungeon.possibleTraits.length)
         {
            m:
            for (var j = 0; j < dungeon.possibleTraits.length; j++)
            {
               for (var k = 0; k < monster.traits.length; k++)
               {                  
                  if (dungeon.possibleTraits[j] !== monster.traits[k].id)
                  {                   
                     monster.AddTrait(new Trait(dungeon.possibleTraits[j]))
                     break m
                  }
               }
            }
         }
         else
         {
            while(same)
            {
               same = false
               var traitId = uMisc.Random(1,uPreferences.NUMBER_OF_TRAITS)        
               for (var j = 0; j < monster.traits.length; j++)
               {         
                  if (monster.traits[j].id == traitId)
                  {
                     same = true
                     break
                  }
               }
               n++
               if (n > 20)
                  break f
            }
            monster.AddTrait(new Trait(traitId))
         }
      }
   }
   if (questBoss)
   {
      monster.sp = "t"
      monster.glory = uBigs.ZERO
      monster.type = game.q.ar.mt
   }
   return monster
}

// ex.GetMonster = function(level)
// {
   // var m = {}
   // m.type = uMisc.Random(0,uPreferences.NUMBER_OF_MONSTERS)
   // if (level < PRECREATE_MONSTER_LEVELS)
   // {
      // m.hp = monstersHp[level-1]
      // m.coinsData = GetCoinsData(monstersGold[level-1])
   // }
   // else
   // {
      // m.hp = GetHp(level)
      // monstersHp.push(m.hp)
      // var gold = GetGold(level)
      // monstersGold.push(gold)
      // m.coinsData = GetCoinsData(gold)
   // }
 
   // return m
// }
