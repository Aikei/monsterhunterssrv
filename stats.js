var uBig = require('big.js')

module.exports = function(load)
{
   if (load)
   {
      this.monstersKilled = new uBig(load.monstersKilled)
      this.rareMonstersKilled = new uBig(load.rareMonstersKilled)
      this.eliteMonstersKilled = new uBig(load.eliteMonstersKilled)
      this.totalGoldCollected = new uBig(load.totalGoldCollected)
      this.totalHeroesLevel = new uBig(load.totalHeroesLevel)
      this.maxDungeonLevel = new uBig(load.maxDungeonLevel)
      //this.totalDungeonLevels = new uBig(load.totalDungeonLevels)      
      this.totalSkillsPicked = new uBig(load.totalSkillsPicked)
      this.monstersRan = new uBig(load.monstersRan)
      this.afflictionsSuffered = new uBig(load.afflictionsSuffered)
   }
   else
   {
      this.monstersKilled = new uBig(0)
      this.rareMonstersKilled = new uBig(0)
      this.eliteMonstersKilled = new uBig(0)
      this.totalGoldCollected = new uBig(0)
      this.totalHeroesLevel = new uBig(0)
      this.maxDungeonLevel = new uBig(0)
      //this.totalDungeonLevels = new uBig(0)
      this.totalSkillsPicked = new uBig(0)
      this.monstersRan = new uBig(0)
      this.afflictionsSuffered = new uBig(0)
   }
}
