//message.js
var uMisc = require('../misc')

var ex = module.exports

ex.kMessageTerminator = "\n"
ex.kNullCharacter = "\0"

ex.Names =
{
   kTime : "a",
   kCommand: "b",
   kResponseToCommand: "c",
   kFirstConnect: "x",
}

ex.Message = function(n,s,data)
{
   this.n = n
   this.s = s
   this.m_data = data
}

ex.SendMessage = function(socket, msg)
{
   socket.write(uMisc.Serialize(msg)+ex.kMessageTerminator+ex.kNullCharacter)
}