//stationLogic
//var uGame = require('../game')
var uMessage = require('./message')
var uDbg = require('../debug')
var uMisc = require('../misc')
var uFs = require('fs')
var uBig = require('big.js')


var main = 
{ 
   profiles: null,
   leaderboardEntries: null,
   globalStats: null
}


function Player(profile_data, leaderboard_entry)
{
   this.profile = profile_data
   this.profile.game.glory = new uBig(this.profile.game.glory)
   //this.profile.game = new uGame.Game(null,this.profile.game)
   
   this.leaderboard_entry = leaderboard_entry
   
   this.leaderboard_entry.glory = new uBig(this.leaderboard_entry.g)
   delete this.leaderboard_entry.g
   
   this.leaderboard_entry.daily_glory = new uBig(this.leaderboard_entry.dg)
   delete this.leaderboard_entry.dg
   
   this.leaderboard_entry.player_name = this.leaderboard_entry.n
   delete this.leaderboard_entry.n
   
   this.leaderboard_entry.group_name = this.leaderboard_entry.gn
   delete this.leaderboard_entry.gn

   this.leaderboard_entry.position = this.leaderboard_entry.p
   delete this.leaderboard_entry.p

   this.leaderboard_entry.daily_position = this.leaderboard_entry.dp
   delete this.leaderboard_entry.dp     
}

function LoadAllProfiles(callback)
{
   main.profiles = []
   uFs.readdir('../data/profiles/', function(err,files)
   {
      if (err)
      {
         uDbg.LogSyncAll('.Error reading dir:',err)
         throw err
      }
      if (files.length === 0)
      {
            uDbg.LogSyncAll('.No files found')
            if (callback)
               callback()
            return
      }
      var n = 0
      for (var i = 0; i < files.length; i++)
      {
         uFs.readFile('../data/profiles/'+files[i], function(err,data)
         {
            if (err)
            {
               uDbg.LogSyncAll('Error reading file',files[i])
               throw err
            }
            var p = JSON.parse(data)
            main.profiles.push(p)
            n++
            if (n >= files.length)
            {
               if (callback)
                  callback()
            }
         })
      }
   })
}

function LoadLeaderboardEntries(callback)
{
   try
   {
      main.leaderboardEntries = uFs.readFileSync('../data/leaderboard')
      main.leaderboardEntries = JSON.parse(main.leaderboardEntries)
   }
   catch(error)
   {
      console.log('error found, list is: ', main.leaderboardEntries)
   }
   finally
   {
      if (callback)
         callback()
   }   
}

function LoadGlobalStats(callback)
{
   uFs.readFile('../data/globalStats', function(err,data)
   {
      if (err)
      {
         console.log('Error reading file ../data/globalStats')
         throw err
      }
      main.globalStats = JSON.parse(data)
      if (callback)
         callback()
   })
   
}

function LoadAll()
{
   console.log('executing LoadAll()')
   LoadAllProfiles(LoadLeaderboardEntries(LoadGlobalStats(function () { console.log('LoadAll() done') } )))
}

module.exports.Update = function()
{
   uDbg.LogSyncAll('Update()')
   LoadAllProfiles(LoadPlayers)
}

function Get(ar,command)
{
   var result = []
   var compareResult = false
   var obj = null
   command = "compareResult = ("+command+")"
   //for (var i = 0; i < ar.length; i++)
   for (var key in ar)
   {
      obj = ar[key]
      eval(command)
      if (compareResult)
         result.push(ar[key])
   }
   return result
}

function Merge(ar,cmd)
{
   var result = []
   var obj = null
   var prop = null
   cmd = "prop = "+cmd
   for (var key1 in ar)
   {
      obj = ar[key1]
      eval(cmd)
      if (typeof prop == "array")
         result = result.concat(prop)
      else
      {
         for (var key2 in prop)
         {
            result.push(prop[key2])
         }
      }
   }
   return result
}

function RegisteredAt(date)
{
   var date = date.split(".")
   var d = Number(date[0])
   var m = Number(date[1])
   var y = Number(date[2])
   var result = []
   for (var key in main.globalStats.profileStats)
   {
      var regDate = main.globalStats.profileStats[key].regDate
      if (regDate.d == d && regDate.m == m && regDate.y == y)
      {
         result.push(main.globalStats.profileStats[key])
      }
   }
   return profile
}

function Average(ar,command,l)
{
   var total = 0
   var obj = null
   if (typeof ar == "array")
      l = ar.length
   else if (!l)
      return null
   for (var key in ar)
   {
      obj = ar[key]
      eval("total += "+command)
   }
   total = total / l
   return total
}

function Send(what,socket)
{
   var m = new uMessage.Message(uMessage.Names.kResponseToCommand,null,what)
   uMessage.SendMessage(socket,m)
}

module.exports.RespondToCommand = function(socket, cmd)
{
   var result = null
   cmd = "result = "+cmd
   //console.log('full command:',cmd)
   eval(cmd)
   //console.log('result: ',result)
   Send(result,socket)
}