var uMisc = require('../misc')
var uNet = require('net')
var uDbg = require('../debug')
var uStationLogic = require('./stationLogic')
var uMessage = require('./message')

var port = 8550

var lastId = 10

function GetPolicyResponse(port)
{
  var xml = '<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM'
          + ' "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n<cross-domain-policy>\n';
 
  xml += '<allow-access-from domain="*" to-ports="*"/>\n';
  xml += '<site-control permitted-cross-domain-policies="master-only"/>\n';
  xml += '</cross-domain-policy>\n\0';
 
  return xml;
}

uDbg.Init('csLog.txt')

var server = uNet.createServer(function(socket)
{
	socket.setEncoding('utf8')
	var socketData = ""
	
	socket.on('data', function(data)
	{
      if (socket.myid == undefined)
      {
         socket.myid = lastId++
      }
		socketData += uMisc.DeleteNullChars(data)
		if (socketData === '<policy-file-request/>')
		{
			var xml = GetPolicyResponse(port)			
			socket.write(xml)
			socketData = ""
			return
		}
               
		var substrings = socketData.split(uMessage.kMessageTerminator)
		var lastMsg = substrings.length-1

		if (substrings[lastMsg].length !== 0 && substrings[lastMsg] !== kNullCharacter)
		{
			// partial data read, store for later
			socketData = substrings[lastMsg]
		}
		else
		{
			//full read, clear buffer
			socketData = ""
		}
 
		// process all messages
      var wrongMessage = false
		for ( var i = 0; i<lastMsg; i++ )
		{
			var message = substrings[i];
         var obj = uMisc.DeSerialize(message)
         if (obj === null)
         {
            wrongMessage = true
            continue
         }         
         // process message!
         if (obj.n === uMessage.Names.kTime)
         {
            OnTimeMessage(socket,obj)
         }
         else if (obj.n === uMessage.Names.kCommand)
         {
            console.log('got command: ',obj.m_data)
            uStationLogic.RespondToCommand(socket, obj.m_data)
         }
		}
	});
 
	socket.on('close', function()
   {
      uDbg.Log('_ii','Close received')
      uDbg.Log('_ii','_____________________________________________')
   })
 
	socket.on('timeout', function(data)
	{
		uDbg.Log("timeout received");
	});
 
	socket.on('error', function(data)
	{
		uDbg.Log("error received");
	});
});

function OnTimeMessage(socket,obj)
{
   var serverTime = uMisc.GetServerTime()
   var timeToServer = serverTime - obj.m_data.m_clientTime;
   obj.m_data.m_serverTime = serverTime
   uMessage.SendMessage(socket,obj)
}

server.listen(port)

process.on('uncaughtException', function(err) 
{
   uDbg.LogSync("_ii","Uncaught exception: ", err)
   uDbg.LogSync("_ii","Exception stack: ", err.stack)
});


// function Update()
// {
   // uStationLogic.Update()
   // setTimeout(uStationLogic.Update,10000)
// }

// Update()

