{
   "heroes" :
   {
      "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level (e.g. it costs 10 gold to upgrade from level 1 to level 2)",
      "0" :    { "cost" : "5", "dps" : "0" },
      "1" :    { "cost" : "10", "dps" : "10" },
      "2" :    { "cost" : "20", "dps" : "20" },
      "3" :    { "cost" : "30", "dps" : "30" },
      "4" :    { "cost" : "40", "dps" : "40" },
      "5" :    { "cost" : "50", "dps" : "50" },
      "6" :    { "cost" : "60", "dps" : "60" },
      "7" :    { "cost" : "70", "dps" : "70" },
      "8" :    { "cost" : "80", "dps" : "80" },
      "9" :    { "cost" : "90", "dps" : "90" },  
      "10" :   { "cost" : "100", "dps" : "100" },
      "then" : { "cost" : "110", "dps" : "110" }      
   },
   
   "weapons" :
   {
      "weapon0" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "0", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      },
      
      "weapon1" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "10", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      },
      
      "weapon2" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "20", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      },
      
      "weapon3" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "30", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      },
      
      "weapon4" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "40", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      },
      
      "weapon5" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "50", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      },
      
      "weapon6" :
      {
         "_comment" : "0 - cost of purchase, 1 - n cost of upgrade to next level",
         "0" : { "cost" : "60", "damage" : "0" },
         "1" : { "cost" : "10", "damage" : "1" },
         "2" : { "cost" : "20", "damage" : "2" },
         "then" : { "cost" : "30", "damage" : "3" }
      }    
   },
   
   "monsters" :
   {
      "_comment" : "gold = baseGold*goldMult, hp = baseHp*hpMult",
      "baseGold" : "1",
      "baseHp" : "10",
      "1" : { "goldMult" : "1", "hpMult" : "1" },
      "2" : { "goldMult" : "2", "hpMult" : "2" },
      "3" : { "goldMult" : "3", "hpMult" : "3" },
      "4" : { "goldMult" : "4", "hpMult" : "4" },
      "5" : { "goldMult" : "5", "hpMult" : "5" },
      "6" : { "goldMult" : "6", "hpMult" : "6" },
      "7" : { "goldMult" : "7", "hpMult" : "7" },
      "8" : { "goldMult" : "8", "hpMult" : "8" },
      "9" : { "goldMult" : "9", "hpMult" : "9" },
      "10" : { "goldMult" : "11", "hpMult" : "11" },
      "then" : { "goldMult" : "12", "hpMult" : "12" }
   },
   
   "rare":
   {
      "_comment" : "probability width : [0, 1]",
      "rareMonsterChance" : 0.1,
      "rareMonsterGlory" : 1,
      "rareHpMultiplier" : 2,
      "rareCoinsMultiplier" : 2,
      
      "eliteMonsterChance" : 0.01,
      "eliteMonsterGlory" : 3,
      "eliteHpMultiplier" : 3,
      "eliteCoinsMultiplier" : 3    
   },
   
   "quest" :
   {
      "_comment" : "gold_from_quest = monster_gold_at_current_level * Random(goldMultMin,goldMultMax) * difficultyMult",
      "goldMultMin" : 30,
      "goldMultMax" : 50,
      "easyMult" : 1,
      "moderateMult" : 3,
      "hardMult" : 6,
      "_comment2" : "glory_from_quest = monster_glory_at_current_level * Random(gloryMultMin,gloryMultMax) * difficultyMult",
      "gloryMultMin" : 2,
      "gloryMultMax" : 8      
   },
   
   "goldLimit" :
   {
      "1" : "1000000000",
      "2" : "2000000000",
      "then" : "3000000000"
   }
}