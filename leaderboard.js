//var uAsync = require('async')

// (function() {
    // var childProcess = require("child_process");
    // oldSpawn = childProcess.spawn;
    // function mySpawn() {
        // console.log('spawn called');
        // console.log(arguments);
        // var result = oldSpawn.apply(this, arguments);
        // return result;
    // }
    // childProcess.spawn = mySpawn;
// })();


var uBig = require('big.js')
var uGameProfile = require('./profile/gameProfile')
var spawnChild = require('child_process').spawn
var uPreferences = require('./preferences')
var uFs = require('fs')
var uMisc = require('./misc')
var zlib = require('zlib')
var uSavedData = require('./savedData')

var leaderboard = []
var perDayLeaderboard = []

var deflatedLeaderboard = null
var deflatedPerDayLeaderboard = null

var deflating = false
var halfDeflated = false

var entriesMap = {}
var hashesMap = {}
var hashesSet = new Set()

var sortProcess = spawnChild(uPreferences.NODE_COMMAND,[ 'leaderboardSort.js' ] )
var perDaySortProcess = spawnChild(uPreferences.NODE_COMMAND,[ 'leaderboardSort.js', 'perDay' ] )

sortProcess.stdin.setEncoding('utf8')
sortProcess.stdout.setEncoding('utf8')

perDaySortProcess.stdin.setEncoding('utf8')
perDaySortProcess.stdout.setEncoding('utf8')

var allData = ""
var allPerDaySortData = ""
// var newLeaderBoard = null
// var oldLeaderBoard = null
var sortHalfDone = false
var sorting = false

function StartSorting()
{
   //console.log('starting sort, sending: ',JSON.stringify(leaderboard))
   perDaySortProcess.stdin.write(JSON.stringify(entriesMap)+'\0')
   sortProcess.stdin.write(JSON.stringify(entriesMap)+'\0')
   sorting = true
}

sortProcess.stdout.on('data', function(data)
{
   //console.log('got data: ',data)
   allData += data
   if (data.charAt(data.length-1) == '\0')
   {
      allData = allData.slice(0,allData.length-1)
      AllDataRead()
   }
})

perDaySortProcess.stdout.on('data', function(data)
{
   //console.log('got data: ',data)
   allPerDaySortData += data
   if (data.charAt(data.length-1) == '\0')
   {
      allPerDaySortData = allPerDaySortData.slice(0,allPerDaySortData.length-1)
      AllPerDayDataRead()
   }
})

function AllPerDayDataRead()
{
   //console.log('got end')
   perDayLeaderboard = JSON.parse(allPerDaySortData)
   allPerDaySortData = ""
   for (var i = 0; i < perDayLeaderboard.length; i++)
   {
      perDayLeaderboard[i] = entriesMap[perDayLeaderboard[i].id]
      perDayLeaderboard[i].dp = i
   }
   if (sortHalfDone)
   {
      sorting = false
      sortHalfDone = false
   }
   else
      sortHalfDone = true
   //sortProcess.stdin.write(JSON.stringify(entriesMap)+'\0')   
}

function AllDataRead()
{
   //console.log('got end')
   leaderboard = JSON.parse(allData)
   allData = ""
   for (var i = 0; i < leaderboard.length; i++)
   {
      leaderboard[i] = entriesMap[leaderboard[i].id]
      leaderboard[i].p = i
   }
   if (sortHalfDone)
   {
      sorting = false
      sortHalfDone = false
   }
   else
      sortHalfDone = true
   //sortProcess.stdin.write(JSON.stringify(entriesMap)+'\0')   
}

var Entry = function(id,groupName)
{
   this.id = id
   this.g = new uBig(0)
   this.dg = new uBig(0)
   this.n = uGameProfile._profiles[id].socialProfile.name + ' ' + uGameProfile._profiles[id].socialProfile.lastName
   this.gn = groupName
   this.p = leaderboard.length+1
   this.dp = perDayLeaderboard.length+1
}

module.exports.OnNewDay = function()
{
   for (var i = 0; i < leaderboard.length; i++)
   {
      leaderboard[i].dg = new uBig(0)
   }
}

module.exports.MakeEntry = function(id,groupName)
{
   if (entriesMap[id] == undefined)
   {
      entriesMap[id] = new Entry(id,groupName)
      var h = uMisc.GetNameHash(groupName)
      hashesSet.add(h)
      hashesMap[id] = h
      leaderboard.push(entriesMap[id])
      perDayLeaderboard.push(entriesMap[id])
   }
   else
   {
      entriesMap[id].playerName = uGameProfile._profiles[id].socialProfile.name + ' ' + uGameProfile._profiles[id].socialProfile.lastName
   }
}

module.exports.ChangeGlory = function(id,newGlory,addedGlory)
{
   entriesMap[id].g = newGlory
   entriesMap[id].dg = entriesMap[id].dg.plus(addedGlory)
}

module.exports.ChangeGroupName = function(id,newGroupName)
{
   var h = uMisc.GetNameHash(newGroupName)
   if (hashesSet.has(h))
      return false
   hashesSet.delete(hashesMap[id])
   hashesSet.add(h)
   hashesMap[id] = h
   entriesMap[id].gn = newGroupName
   return true
}

module.exports.SaveLeaderboard = function()
{
   uFs.writeFile('data/leaderboard', JSON.stringify(leaderboard), function(err)
   {      
      if (err)
         throw err
   })
   
   // if (deflatedLeaderboard)
   // {
      // uFs.writeFile('data/deflatedLeaderboard', deflatedLeaderboard, function(err)
      // {      
         // if (err)
            // throw err
      // })
   // }
   
   if (!deflating)
      DeflateLeaderboard()
   // uFs.writeFile('data/perDayLeaderboard', JSON.stringify(leaderboard), function(err)
   // {      
      // if (err)
         // throw err
   // })      
}

function DeflateLeaderboard()
{
   deflating = true
   
   var lb = null
   var perDayLb = null
   
   if (leaderboard.length > 100)
   {
      lb = JSON.stringify(leaderboard.slice(0,100))
      perDayLb = JSON.stringify(perDayLeaderboard.slice(0,100))
   }
   else
   {
      lb = JSON.stringify(leaderboard)
      perDayLb = JSON.stringify(perDayLeaderboard)
   }
   
   zlib.deflate(lb, function(err, buffer)
   {
      if (halfDeflated)
      {
         deflating = false
         halfDeflated = false
      }
      else
         halfDeflated = true
      if (!err)
      {
         deflatedLeaderboard = buffer.toString('base64')
      }
      else
      {
         console.log(err)
      }
   })
   
   zlib.deflate(perDayLb, function(err, buffer)
   {
      if (halfDeflated)
      {
         deflating = false
         halfDeflated = false
      }
      else
         halfDeflated = true      
      if (!err)
      {
         deflatedPerDayLeaderboard = buffer.toString('base64')
      }
      else
      {
         console.log(err)
      }
   })   
}

module.exports.LoadLeaderboard = function()
{
   try
   {
      leaderboard = uFs.readFileSync('data/leaderboard')
      leaderboard = JSON.parse(leaderboard)
   }
   catch(error)
   {
      leaderboard = []
   }
   finally
   {
      entriesMap = {}
      for (var i = 0; i < leaderboard.length; i++)
      {
         if (uSavedData.IsCheater(leaderboard[i].id))
         {
               leaderboard.splice(i,1)
               i--
               continue
         }               
         leaderboard[i].g = new uBig(leaderboard[i].g)
         leaderboard[i].dg = new uBig(leaderboard[i].dg)
         entriesMap[leaderboard[i].id] = leaderboard[i]
         var h = uMisc.GetNameHash(leaderboard[i].gn)
         hashesSet.add(h)
         hashesMap[leaderboard[i].id] = h         
      }
   }
   
   // try
   // {
      // perDayLeaderboard = uFs.readFileSync('data/perDayLeaderboard')
      // perDayLeaderboard = JSON.parse(perDayLeaderboard)
   // }
   // catch(error)
   // {
      // perDayLeaderboard = []
      // return
   // }   
}

function CompareEntries(entryA,entryB)
{
   if (entryA.glory.gt(entryB.glory))
      return -1
   else
      return 1
}

module.exports.Init = function()
{
   module.exports.LoadLeaderboard()
}

module.exports.GetDeflatedLeaderboard = function(profileId)
{
   var lb = { }
   lb.l = deflatedLeaderboard
   lb.dl = deflatedPerDayLeaderboard
   if (profileId)
      lb.u = entriesMap[profileId]
   return lb
}

module.exports.GetLeaderboard = function(profileId)
{
   var lb = { }
   lb.l = leaderboard
   lb.dl = perDayLeaderboard
   if (profileId)
      lb.u = entriesMap[profileId]
   return lb
}

// module.exports.GetLeaderboard = function(profileId)
// {
   // var lb = { }
   // if (leaderboard.length > 100)
   // {
      // lb.leaderboard = leaderboard.slice(0,100)
      // lb.perDayLeaderboard = perDayLeaderboard.slice(0,100)
   // }
   // else
   // {
      // lb.leaderboard = leaderboard
      // lb.perDayLeaderboard = perDayLeaderboard
   // }
   // lb.self = entriesMap[profileId]
   // return lb
// }

module.exports.Update = function()
{
   if (!uPreferences.shuttingDown)
   {
      if (!sorting)
         StartSorting()
      module.exports.SaveLeaderboard()
   }
}
