const PRECREATE_LEVELS = 200

var uBig = require('big.js')
var uPreferences = require('./preferences')
var uMisc = require('./misc')
var uHeroes = require('./heroes')
var uBalance = require('./balanceData')

var ex = module.exports

var weapons = []

const BASE_HUMAN_CLICKS_PER_SECOND = 5
const BASE_HUMAN_CLICKS_PER_SECOND_BIG = new uBig(BASE_HUMAN_CLICKS_PER_SECOND)

var onePointZeroSeven = new uBig(1.07)

//var dpsDivider = new uBig(5)

// var weaponsDamageCostBase =      [        new uBig(0.1),    new uBig(3),     new uBig(5),        new uBig(5),        new uBig(5),           new uBig(5),                 new uBig(5) ]
// var weaponsDamageCostExponent =  [        new uBig(1.4),   new uBig(1.3),    new uBig(1.2),      new uBig(1.2),      new uBig(1.15),          new uBig(1.15),               new uBig(1.1) ]
// var weaponsStartDps =            [        new uBig(5),      new uBig(150),    new uBig(10000),     new uBig(770000),    new uBig("135000000"),  new uBig("162000000000"),     new uBig("184000000000000")]
// var weaponsDamageIncrease =      [        new uBig(1.2),    new uBig(1.05),    new uBig(1.05),       new uBig(1.05),       new uBig(1.05),          new uBig(1.05),                new uBig(1.05) ]

// var weaponsDamageCostBase =      [        new uBig(0.1),    new uBig(10),     new uBig(10),        new uBig(10),        new uBig(10),           new uBig(10),                 new uBig(10) ]
// var weaponsDamageCostExponent =  [        new uBig(1.45),   new uBig(1.3),    new uBig(1.2),      new uBig(1.25),      new uBig(1.2),          new uBig(1.15),               new uBig(1.1) ]
// var weaponsStartDps =            [        new uBig(5),      new uBig(150),    new uBig(10000),     new uBig(770000),    new uBig("135000000"),  new uBig("162000000000"),     new uBig("184000000000000")]
// var weaponsDamageIncrease =      [        new uBig(1.2),    new uBig(1.1),    new uBig(1.1),       new uBig(1.1),       new uBig(1.1),          new uBig(1.1),                new uBig(1.1) ]


// var oneHundred = new uBig(100)
// var nineNundred = new uBig(900)
// var oneThousand = new uBig(1000)

// var one = new uBig(1)
// var oneHundedThousand = new uBig(100000)

// function GetDamageCost(type, level)
// {
   // return weaponsDamageCostBase[type].times(weaponsDamageCostExponent[type].pow(level-1)).round(0,3)
// }

// function GetDps(type,level)
// {
   // return weaponsStartDps[type].times(weaponsDamageIncrease[type].pow(level-1)).round(0,3)
// }

// function GetDamage(type,level)
// {
   // return GetDps(type,level).div(BASE_HUMAN_CLICKS_PER_SECOND).round(0,3).plus(level-1)
// }

// function GetCost(type,level)
// {
   // return GetDamageCost(type,level).times(GetDps(type,level)).round(0,3)
// }

ex.CreateWeapon = function(type,level,noNextLevel)
{
   var w = {}
   w.type = type
   w.damage = {}
   w.damage.type = uPreferences.DAMAGE_TYPE_NONE
   w.damage.value = uBalance.GetWeaponDamage(type,level)
   w.buyCost = uBalance.GetWeaponCost(type,0)
   w.levelUpCost = uBalance.GetWeaponCost(type,level)   
   // w.damage.value = GetDamage(type,level)
   // w.buyCost = GetCost(type,1)
   // w.levelUpCost = GetCost(type,level+1) 
   w.level = level
   if (!noNextLevel)
      w.nextLevelWeapon = ex.CreateWeapon(type,level+1,true)
   return w   
}

ex.Init = function()
{
   // for (var i = 0; i < uPreferences.NUMBER_OF_WEAPONS; i++)
   // {
      // weapons.push([])
      // for (var j = 1; j <= PRECREATE_LEVELS; j++)
      // {
         // weapons[i][j] = ex.CreateWeapon(i,j)
      // }
   // }
}

ex.GetWeapon = function(type, level)
{
   return ex.CreateWeapon(type,level)
}

// ex.GetWeapon = function (type, level)
// {
   // if (level < PRECREATE_LEVELS)
   // {
      // return weapons[type][level]
   // }
   // else
   // {
      // var w = ex.CreateWeapon(type,level)
      // weapons.push(w)
      // PRECREATE_LEVELS++
      // return w
   // }
// }

// const PRECREATE_LEVELS = 200

// var uBig = require('big.js')
// var uPreferences = require('./preferences')
// var uMisc = require('./misc')

// var ex = module.exports

// var weapons = []

// const BASE_HUMAN_CLICKS_PER_SECOND = 5
// const BASE_HUMAN_CLICKS_PER_SECOND_BIG = new uBig(BASE_HUMAN_CLICKS_PER_SECOND)

// var onePointZeroSeven = new uBig(1.07)

// var woodenSwordDamageBase = new uBig(1.05)
// var woodenSwordCostBase = new uBig(1.3)
// var woodenSwordCostIncrease = new uBig(1.50)
// var woodenSwordBaseDamageCost = new uBig(0.2)
// var woodenSwordBuyCost = new uBig(0)
// var woodenSwordSecondCostPlus = new uBig(5)

// var ironSwordBuyCost = new uBig(1000)
// var ironSwordStartDamageLevel = 25
// var ironSwordCostIncrease = new uBig(1.3)
// var ironSwordBaseDamageCost = new uBig(5)
// var ironSwordDamageBase = new uBig(1.15)
// var ironSwordCostBase = new uBig(1.35)
// var ironSwordSecondCostPlus = new uBig(10)

// var steelSwordBuyCost = new uBig(100000)
// var steelSwordStartDamageLevel = 37
// var steelSwordBaseDamageCost = new uBig(10)
// var steelSwordCostIncrease = new uBig(1.11)
// var steelSwordDamageBase = new uBig(1.15)
// var steelSwordCostBase = new uBig(1.4)

// var steelSwordSecondCostPlus = new uBig(20)

// var oneHundred = new uBig(100)
// var nineNundred = new uBig(900)
// var oneThousand = new uBig(1000)

// var one = new uBig(1)
// var oneHundedThousand = new uBig(100000)

// function GetDamageCost(type, level)
// {
   // if (type === uPreferences.WEAPON_WOODEN_SWORD)
   // {
      // return woodenSwordBaseDamageCost.times(woodenSwordCostIncrease.pow(level-1))
   // }
   // else if (type === uPreferences.WEAPON_IRON_SWORD)
   // {
      // return ironSwordBaseDamageCost.times(ironSwordCostIncrease.pow(level-1))
   // }
   // else if (type === uPreferences.WEAPON_STEEL_SWORD)
   // {
      // return steelSwordBaseDamageCost.times(steelSwordCostIncrease.pow(level-1))
   // }	   
// }

// function GetSecondCostPlus(type)
// {
   // if (type === uPreferences.WEAPON_WOODEN_SWORD)
   // {
      // return woodenSwordSecondCostPlus
   // }
   // else if (type === uPreferences.WEAPON_IRON_SWORD)
   // {
      // return ironSwordSecondCostPlus
   // }
   // else if (type === uPreferences.WEAPON_STEEL_SWORD)
   // {
      // return steelSwordSecondCostPlus
   // }		
// }

// function GetCostBase(type)
// {
   // if (type === uPreferences.WEAPON_WOODEN_SWORD)
   // {
      // return woodenSwordCostBase
   // }
   // else if (type === uPreferences.WEAPON_IRON_SWORD)
   // {
      // return ironSwordCostBase
   // }
   // else if (type === uPreferences.WEAPON_STEEL_SWORD)
   // {
      // return steelSwordCostBase
   // }	
// }

// var tenPercentIncrese = new uBig(1.1)

// function GetDamage(type,level)
// {
   // if (type === uPreferences.WEAPON_WOODEN_SWORD)
   // {
      // return woodenSwordDamageBase.pow(level).times(new uBig(level)).round(0,0)
   // }
   // else if (type === uPreferences.WEAPON_IRON_SWORD)
   // {
      // return GetDamage(type-1,ironSwordStartDamageLevel).times(tenPercentIncrese.pow(level-1)).round(0,0)
      ////return ironSwordStartDamage.plus(ironSwordDamageBase.pow(level).times(new uBig(level))).round(0,0)
   // }
   // else if (type === uPreferences.WEAPON_STEEL_SWORD)
   // {
      // return GetDamage(type-1,steelSwordStartDamageLevel).times(tenPercentIncrese.pow(level-1)).round(0,0)
      ////return steelSwordStartDamage.times(tenPercentIncrese.pow(level-1)).round(0,0)
      ////return steelSwordStartDamage.plus(steelSwordCostBase.pow(level).times(new uBig(level))).round(0,0)
   // }   
// }


// function GetBuyCost(type)
// {
   // if (type === uPreferences.WEAPON_WOODEN_SWORD)
   // {
      // return woodenSwordBuyCost
   // }
   // else if (type === uPreferences.WEAPON_IRON_SWORD)
   // {
      // return ironSwordBuyCost
   // }
   // else if (type === uPreferences.WEAPON_STEEL_SWORD)
   // {
      // return steelSwordBuyCost
   // }
// }

// function GetLevelUpCost(type,level)
// {
   // return GetBuyCost(type).plus((GetDamageCost(type,level+1).times(GetDamage(type,level+1).times(BASE_HUMAN_CLICKS_PER_SECOND_BIG)))).round(0,3)
   ////return GetBuyCost(type).plus(GetCostBase(type).pow(level).times(new uBig(5+level))).round(0,0)
// }

// ex.CreateWeapon = function(type,level,noNextLevel)
// {
   // var w = {}
   // w.type = type
   // w.damage = {}
   // w.damage.type = uPreferences.DAMAGE_TYPE_NONE
   // w.damage.value = GetDamage(type,level)
   // w.buyCost = GetBuyCost(type)
   // w.levelUpCost = GetLevelUpCost(type,level)   
   // w.level = level
   // if (noNextLevel == undefined)
      // w.nextLevelWeapon = ex.CreateWeapon(type,level+1,true)
   // return w   
// }

// ex.Init = function()
// {
   // for (var i = 0; i < uPreferences.NUMBER_OF_WEAPONS; i++)
   // {
      // weapons.push([])
      // for (var j = 1; j <= PRECREATE_LEVELS; j++)
      // {
         // weapons[i][j] = ex.CreateWeapon(i,j)
      // }
   // }
// }

// ex.GetWeapon = function (type, level)
// {
   // if (level < PRECREATE_LEVELS)
   // {
      // return weapons[type][level]
   // }
   // else
   // {
      // var w = ex.CreateWeapon(type,level)
      // weapons.push(w)
      // PRECREATE_LEVELS++
      // return w
   // }
// }