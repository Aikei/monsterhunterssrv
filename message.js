//message.js

var uMys = require('./mys.js')
var zlib = require('zlib')

var ex = module.exports

ex.Names =
{
   kTime : "a",
   kCreatePlayer : "b",
   kReady : "c",
   kCheater: "cheat",
   kServerInfo: "si",
   kListPlayers: "lp",
   kServerMessage: "k",
   kNewDay: "n",
   kPreferences: "prefs",
   kConfirmConnect: "v",
   kFirstConnect: "x",  
   
   kGameMessage: "g",
      ksTutorialDone: "aa",
      ksChangeLevel: "a",
      ksQuestAccepted: "b",
      ksQuestRefused: "c",      
      ksMonsterKilled: "d",
      ksNewMonster: "e",
      ksCoinsReceived: "f",
      ksWeaponUpgraded: "g",
      ksWeaponBought: "h",
      ksNextLevel: "i",
      ksChangeDungeon: "j",
      ksStartGame: "k",
      ksBuyHero: "l",
      ksUpgradeHero: "m",
      ksSkillPicked: "n",
      ksMonsterRan: "o",
      ksAfflictionAdded: "p",
      ksAfflictionRemoved: "q",
      ksNewSkillPack: "r",
      ksNextSkillPackAvailable: "s",
      ksRequestLeaderboard: "t",
      ksSquadNameChanged : "u",
      
      ksRareLoaded: "vv",
      ksGloryReceived: "w",
      ksQuestProgress: "x",
      ksRenameResponse: "y",
	   ksBgReportShared: "z"
}

ex.Message = function(n,s,data)
{
   this.n = n
   this.s = s
   this.m_data = data
   this.z = false
}

ex.GameMessage = function(s,data)
{
   this.n = ex.Names.kGameMessage
   this.s = s
   this.m_data = data
   this.z = false
}

ex.SendMessage = function(socket, msg, z)
{
   if (z)
   {
      msg.z = true
      zlib.deflate(JSON.stringify(msg.m_data), function(err, buffer)
      {
         msg.m_data = buffer.toString('base64')
         uMys.SendData(socket,msg)
      })      
   }
   else
      uMys.SendData(socket,msg)
}

ex.SendCheater = function(socket,data)
{
   var m = new ex.Message(ex.Names.kCheater,undefined,data)
   ex.SendMessage(socket,m) 
}

// ex.GameStartMessage = function(yourNumber, gameId, firstTurn, field, entropy, dice, players)
// {
   // this.n = ex.Names.kGameMessage
   // this.s = ex.Names.ksGameStart
   // this.m_data = {}
   // this.m_data.number = yourNumber
   // this.m_data.firstTurn = firstTurn
   // this.m_data.field = field
   // this.m_data.maxHP = uPrefs.maxHP
   // this.m_data.gameId = gameId
   // this.m_data.entropy = entropy
   // this.m_data.dice = dice
   // this.m_data.abilities = []
   // this.m_data.heroClasses = []
   // for (var i = 0; i < players.length; i++)
   // {
      // this.m_data.heroClasses.push(players[i].heroClass)
      // this.m_data.abilities.push([])
      // for (var key in players[i].abilities)
      // {
         // this.m_data.abilities[i].push(players[i].abilities[key].type)         
      // }
      // if (i !== yourNumber)
      // {
         // this.m_data.enemyProfile = players[i].profile
      // }
      // else
      // {
         // this.m_data.yourProfile = players[i].profile
      // }
   // }                  
// }

// ex.CheatMessage = function()
// {
   // this.n = ex.Names.kCheater
   // this.s = undefined
   // this.m_data = undefined
// }

// ex.EntropyMessage = function(entropy,dice,gameId)
// {
   // this.n = ex.Names.kGameMessage
   // this.s = ex.Names.ksNewEntropy
   // this.m_data = {}
   // this.m_data.gameId = gameId
   // this.m_data.entropy = entropy
   // this.m_data.dice = dice
// }

// ex.NextTurnMessage = function(game)
// {
   // this.n = ex.Names.kGameMessage
   // this.s = ex.Names.ksNextTurn
   // this.m_data = {}
   // this.m_data.turn = game.turn
   // this.m_data.gameId = game.id
   // this.m_data.entropy = game.entropy
   // this.m_data.dice = game.dice
   // this.m_data.turnNumber = game.turnNumber
// }

// ex.SendText = function(socket, text)
// {
   // var msg = new ex.Message(ex.Names.kServerMessage,undefined,text)
   // ex.SendMessage(socket,msg)
// }

// ex.SendRefreshPlayerData = function(plr,players,gameId,turnStart)
// {
   // var msg = new ex.Message(ex.Names.kGameMessage, ex.Names.ksRefreshPlayerData, {})
   // msg.m_data.gameId = gameId
   // msg.m_data.playerData = []
   // if (turnStart != undefined)
      // msg.m_data.turnStart = true
   // else
      // msg.m_data.turnStart = false
   // for (var i = 0; i < players.length; i++)
   // {
      // msg.m_data.playerData.push( {} )
      // msg.m_data.playerData[i].hp = players[i].hp
      // msg.m_data.playerData[i].shield = players[i].shield
      // msg.m_data.playerData[i].mana = players[i].mana
      // msg.m_data.playerData[i].lastDamageToShield = players[i].lastDamageToShield
      // msg.m_data.playerData[i].abilities = []
      // for (var key in players[i].abilities)
      // {
         // msg.m_data.playerData[i].abilities.push( { type : players[i].abilities[key].type, charge : players[i].abilities[key].currentCharge } )
      // }
      // if (i === plr.number)
         // msg.m_data.playerData[i].isEnemy = false
      // else
         // msg.m_data.playerData[i].isEnemy = true
     ////msg.m_data.playerData[i].heroClass = players[i].heroClass
   // }
   // ex.SendMessage(plr.socket, msg)   
// }

// ex.SendNoMoves = function(game,usedEntropy,usedDice,whoNumber,causedByPlayer)
// {
   // var msg = new ex.Message(ex.Names.kGameMessage, ex.Names.ksNoMoves, {})
   // msg.m_data.entropy = game.entropy
   // msg.m_data.usedEntropy = usedEntropy
   // msg.m_data.gameId = game.id
   // msg.m_data.dice = game.dice
   // msg.m_data.usedDice = usedDice
   // msg.m_data.whoNumber = whoNumber
   // msg.m_data.causedByPlayer = causedByPlayer
   // for (var i = 0; i < game.players.length; i++)
   // {
      // ex.SendMessage(game.players[i].socket, msg)
   // }
// }

// ex.SendHpNow = function(socket,hpObj,gameId)
// {
   // var msg = new ex.Message(ex.Names.kGameMessage, ex.Names.ksHpNow, {})
   // msg.m_data.gameId = gameId
   // msg.m_data.hp = hpObj
   // ex.SendMessage(socket, msg)
// }